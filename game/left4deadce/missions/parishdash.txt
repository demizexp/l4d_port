"mission"
{
	"Name"			"parishdash"
	"Version"		"1"
	"Author"		"Valve"
	"Website"		"http://store.steampowered.com"
	"BuiltIn"		"0"
	
	"DisplayTitle"	"Parish Dash"
	"Description"	"Two Dash maps set in the Parish"	
	
	"Image"		"maps/any"
	"OuttroImage"	"vgui/Left4Dead2Logo"
	
	"meleeweapons"	"fireaxe;machete;tonfa;katana;baseball_bat;knife"
	
	"survivor_set"		"1"		// Which set of survivors should be used. 1=L4D1, 2=L4D2

	"modes"
	{
		"dash"
		{
			"1"
			{
				"Map" "c5m2_park"
				"DisplayName" "#L4D360UI_LevelName_COOP_C5M2"
				"Image" "maps/c5m2_park"
			}
			"2"
			{
				"Map" "c5m4_quarter"
				"DisplayName" "#L4D360UI_LevelName_COOP_C5M4"
				"Image" "maps/c5m4_quarter"
			}	
		}
	}
}











