@echo off
setlocal

set exefile=%1
set exefile=%exefile:"=%

set extraparams="%2"
set extraparams=%extraparams:"=%
if "%extraparams%" == "" (
	set extraparams=
)

set exedir="%3"
set exedir=%exedir:"=%
if "%exedir%" == "" (
	set exedir="%ProgramFiles(x86)%\Steam\steamapps\common\Alien Swarm\bin"
) else (
	set exedir="%exedir%"
)

set game="%4"
set game=%game:"=%
if "%game%" == "" (
	set game="%~dp0.."
) else (
	set game="%game%"
)

set params=-nop4 -override_vpk -game %game% %extraparams%

cd %exedir%
start "" /D%exedir% /B /SHARED /HIGH %exefile% %params%

endlocal
exit