WeaponData
{
	"VerticalPunch"			"2.0"
	"DeployDuration"		"0.5"

	"MaxPlayerSpeed"		"250"
	"WeaponType"			"melee"
	"WeaponArmorRatio"		"1.0"
	"CrosshairMinDistance"		"8"
	"CrosshairDeltaDistance" 	"3"
	"Team" 					"Survivor"
	"BuiltRightHanded" 		"1"
	"PlayerAnimationExtension" 	"knife"
	
	"Rumble"			"1"
	
	"CycleTime"			"0.175"
	
	"ResponseRulesName"		"Melee"
	
	// Weapon data is loaded by both the Game and Client DLLs.
	"printname"			"BaseMelee"
	"viewmodel"			"models/weapons/melee/v_knife.mdl"
	"playermodel"		"models/weapons/melee/w_knife.mdl"
	
	"AddonAttachment"			"primary"

	"anim_prefix"			"anim"
	"bucket"				"1"
	"bucket_position"		"1"
	
	"weight"				"5"
	"item_flags"			"0"
	
	"refire_delay"	"0.8"
	
	// Damage flag value
	// right now you can enter the integer values.
	//DMG_SLASH			(1 << 2) = 4 - cuts off parts of infected
	//DMG_BURN			(1 << 3) = 8 - lights zombies on fire
	//DMG_CLUB			(1 << 7) = 128 - knocks them back?
	"damage_flags"	"4"
	
	
	// How long after attacking until the weapon enters its idle animation state
	"weapon_idle_time"	"0.5"
	
	
	// Attack animations (primary and secondary)
	"primaryattacks"
	{
		//"stab1"
		//{
		//	"startdir"		"E"
		//	"enddir"		"W"
		//	"duration"		"1.1"
		//	"starttime"		"0.05"
		//	"endtime"		"0.35"
		//	"activity"		"ACT_VM_PRIMARYATTACK"
		//	"player_activity" "ACT_SHOOT_E2W_AXE"
		//	"player_activity_idle"	"ACT_SHOOT_E2W_IDLE_AXE"
		//	"force_dir"		"20 10 0"
		//}
		"slash1"
		{
			"startdir"		"W"
			"enddir"		"E"
			"duration"		"1.1"
			"starttime"		"0.05"
			"endtime"		"0.35"
			"activity"		"ACT_VM_HITRIGHT"
			"player_activity" "ACT_SHOOT_W2E_AXE"
			"player_activity_idle"	"ACT_SHOOT_W2E_IDLE_AXE"
			"force_dir"		"8 -4 0"
		}
	}

	"secondaryattacks"
	{
		"elbow1"
		{
			"startdir"		"W"
			"enddir"		"E"
			"duration"		".7"
			"starttime"		"0.08"
			"endtime"		"0.4"
			"activity"		"ACT_VM_SECONDARYATTACK"
			"player_activity"	"ACT_SHOOT_SECONDARY_AXE"
			"player_activity_idle"	"ACT_SHOOT_SECONDARY_AXE"
		}
	}

	// hud textures
	"sprite_active"		"icon_knife"
	
	// Sounds for the weapon. There is a max of 16 sounds per category (i.e. max 16 "single_shot" sounds)
	SoundData
	{
		"melee_miss"			"Melee.Miss"
		"melee_hit"				"Machete.ImpactFlesh"
		"melee_hit_world"	    "Machete.ImpactWorld"
	}

	// Weapon Sprite data is loaded by the Client DLL.
	TextureData
	{
		"weapon"
		{
				"font"		"L4D_WeaponsSmall"
				"character"	"%"
		}
		"weapon_s"
		{	
				"font"		"L4D_Weapons"
				"character"	"%"
		}
		"weapon_dual"
		{
				"font"		"L4D_WeaponsSmall"
				"character"	"&"
		}
		"weapon_dual_s"
		{	
				"font"		"L4D_Weapons"
				"character"	"&"
		}
		"ammo"
		{
				"font"		"DebugFixed"
				"character"	"A"
		}
		"crosshair"
		{
				"file"		"sprites/crosshairs"
				"x"			"0"
				"y"			"48"
				"width"		"24"
				"height"	"24"
		}
		"autoaim"
		{
				"file"		"sprites/crosshairs"
				"x"			"0"
				"y"			"48"
				"width"		"24"
				"height"	"24"
		}
	}
	ModelBounds
	{
		Viewmodel
		{
			Mins	"-7 -4 -14"
			Maxs	"24 9 -2"
		}
		World
		{
			Mins	"-1 -4 -3"
			Maxs	"17 5 6"
		}
	}
}
