#include "cbase.h"
#include "gameinterface.h"
#include "mapentities.h"
#include "fmtstr.h"

extern const char *COM_GetModDirectory();
extern ConVar sv_force_transmit_ents;

void CServerGameClients::GetPlayerLimits(int &minplayers, int &maxplayers, int &defaultMaxPlayers) const
{
	minplayers = 4; 
	defaultMaxPlayers = 9;
	maxplayers = 32;
}

void CServerGameDLL::LevelInit_ParseAllEntities(const char *pMapEntities)
{
	
}

void CServerGameDLL::ApplyGameSettings(KeyValues *pKV)
{
	static bool g_bOfflineGame = false;

	if(!pKV)
		return;

	bool bAlreadyLoadingMap = false;
	const char *szBspName = nullptr;
	const char *pGameDir = COM_GetModDirectory();
	if(V_stricmp(pKV->GetName(), "::ExecGameTypeCfg") == 0)
	{
		if(!engine)
			return;

		const char *szNewMap = pKV->GetString("map/mapname", "");
		if(!szNewMap || !*szNewMap)
			return;

		KeyValues *pLaunchOptions = engine->GetLaunchOptions();
		if(!pLaunchOptions)
			return;

		if(FindLaunchOptionByValue(pLaunchOptions, "changelevel") || FindLaunchOptionByValue(pLaunchOptions, "changelevel2"))
			return;
		if(FindLaunchOptionByValue(pLaunchOptions, "map_background"))
			return;

		bAlreadyLoadingMap = true;

		if(FindLaunchOptionByValue(pLaunchOptions, "reserved"))
		{
			pKV->SetInt( "members/numSlots", g_bOfflineGame ? 1 : 4 );
			return;
		}

		pKV->SetName(pGameDir);
	}

	if(V_stricmp(pKV->GetName(), pGameDir) != 0 || bAlreadyLoadingMap)
		return;

	g_bOfflineGame = (V_stricmp(pKV->GetString("system/network", "LIVE"), "offline") == 0);

	if(sv_force_transmit_ents.IsFlagSet(FCVAR_DEVELOPMENTONLY) && sv_cheats && sv_cheats->GetBool())
		sv_cheats->SetValue(0);

	const char *szMapCommand = pKV->GetString("map/mapcommand", "map");
	const char *szMode = pKV->GetString("game/mode", "campaign");

	const char *szGameMode = pKV->GetString("game/mode", "");
	if(szGameMode && *szGameMode)
	{
		extern ConVar mp_gamemode;
		mp_gamemode.SetValue(szGameMode);
	}

	if(V_stricmp(szMode, "campaign") == 0)
	{
		const char *szCampaignName = pKV->GetString("game/campaign", nullptr);
		if(!szCampaignName)
			return;

		const char *szStartingMission = pKV->GetString("game/mission", nullptr);

		engine->ServerCommand(CFmtStr("%s %s campaign %s reserved\n", szMapCommand, szStartingMission ? szStartingMission : "lobby", ""));
	}
	else if(V_stricmp(szMode, "single_mission") == 0)
	{
		szBspName = pKV->GetString("game/mission", nullptr);
		if(!szBspName)
			return;

		engine->ServerCommand(CFmtStr("%s %s reserved\n", szMapCommand, szBspName));
	}
	else
	{
		Warning("ApplyGameSettings: Unknown game mode!\n");
	}
}