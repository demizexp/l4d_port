#include "cbase.h"
#include "l4dce_viewcontrol.h"

BEGIN_DATADESC( CSurvivorCamera )
	DEFINE_INPUTFUNC( FIELD_VOID, "StartMovement", InputStartMovement ),
	DEFINE_FUNCTION(GoToPlayer)
END_DATADESC()

LINK_ENTITY_TO_CLASS( point_viewcontrol_survivor, CSurvivorCamera )

void CSurvivorCamera::Enable( void )
{
	m_state = USE_ON;

	if ( !m_hPlayer || !m_hPlayer->IsPlayer() )
	{
		m_hPlayer = UTIL_GetLocalPlayer();
	}

	if ( !m_hPlayer )
	{
		DispatchUpdateTransmitState();
		return;
	}

	Assert( m_hPlayer->IsPlayer() );
	CBasePlayer *pPlayer = NULL;

	if ( m_hPlayer->IsPlayer() )
	{
		pPlayer = ((CBasePlayer*)m_hPlayer.Get());
	}
	else
	{
		Warning("CTriggerCamera could not find a player!\n");
		return;
	}

	CBaseEntity *pPrevViewControl = pPlayer->GetViewEntity();
	if (pPrevViewControl && pPrevViewControl != pPlayer)
	{
		CTriggerCamera *pOtherCamera = dynamic_cast<CTriggerCamera *>(pPrevViewControl);
		if ( pOtherCamera )
		{
			if ( pOtherCamera != this )
			{
				pOtherCamera->Disable();
			}
		}
	}

	if ( HasSpawnFlags( 2 ) )
	{
		if ( pPlayer )
		{
			if ( pPlayer->GetFOVOwner() && (FClassnameIs( pPlayer->GetFOVOwner(), "point_viewcontrol_multiplayer" ) || FClassnameIs( pPlayer->GetFOVOwner(), "point_viewcontrol" )) )
			{
				pPlayer->ClearZoomOwner();
			}
			pPlayer->SetFOV( this, m_fov, m_fovSpeed );
		}
	}

	// If we don't have a m_hMoveParent, ignore the attachment / etc
	if ( m_hMoveParent )
	{
		m_iAttachmentIndex = 0;
		if ( m_iszTargetAttachment != NULL_STRING )
		{
			if ( !m_hMoveParent->GetBaseAnimating() )
			{
				Warning("%s tried to target an attachment (%s) on parent %s, which has no model.\n", GetClassname(), STRING(m_iszTargetAttachment), STRING(m_hMoveParent->GetEntityName()) );
			}
			else
			{
				m_iAttachmentIndex = m_hMoveParent->GetBaseAnimating()->LookupAttachment( STRING(m_iszTargetAttachment) );
				if ( !m_iAttachmentIndex )
				{
					Warning("%s could not find attachment %s on parent %s.\n", GetClassname(), STRING(m_iszTargetAttachment), STRING(m_hMoveParent->GetEntityName()) );
				}
			}
		}
	}

	SetAbsVelocity( vec3_origin );

	pPlayer->SetViewEntity( this );

	// Hide the player's viewmodel
	if ( pPlayer->GetActiveWeapon() )
	{
		pPlayer->GetActiveWeapon()->AddEffects( EF_NODRAW );
	}

	m_moveDistance = 0;
	m_flSpeed = 300;

	DispatchUpdateTransmitState();
}

void CSurvivorCamera::Disable( void )
{
	if ( m_hPlayer )
	{
		CBasePlayer *pBasePlayer = (CBasePlayer*)m_hPlayer.Get();

		pBasePlayer->SetViewEntity( NULL );
		pBasePlayer->m_Local.m_bDrawViewmodel = true;

	}

	m_state = USE_OFF;
	m_flReturnTime = gpGlobals->curtime;
	SetThink( NULL );

	SetLocalAngularVelocity( vec3_angle );

	DispatchUpdateTransmitState();
}

void CSurvivorCamera::InputStartMovement( inputdata_t &inputdata )
{
	m_flReturnTime = gpGlobals->curtime + 1.0;
	SetThink(&CSurvivorCamera::GoToPlayer);
	SetNextThink(gpGlobals->curtime);
}

void CSurvivorCamera::GoToPlayer()
{
	SetParent(NULL);
	if (m_flReturnTime != 0 && m_flReturnTime < gpGlobals->curtime)
	{
		Disable();
		return;
	}
	if ( !m_hPlayer )
	{
		Disable();
		return;
	}
	CBasePlayer *pPlayer = (CBasePlayer*)m_hPlayer.Get();
	if ( !pPlayer )
		return;

	QAngle vecGoal = pPlayer->LocalEyeAngles();
	SetLocalAngles(vecGoal);

	SetNextThink(gpGlobals->curtime);

	SetMoveType(MOVETYPE_NOCLIP);

	// Subtract movement from the previous frame
	m_moveDistance -= m_flSpeed * gpGlobals->frametime;

	// Have we moved enough to reach the target?
	if ( m_moveDistance <= 0 )
	{
		m_targetSpeed = 50.0;

		m_vecMoveDir = pPlayer->GetLocalOrigin() + pPlayer->GetViewOffset() - GetLocalOrigin();
		m_moveDistance = VectorNormalize( m_vecMoveDir );
		m_flStopTime = gpGlobals->curtime + 1.0;
	}

	if ( m_flStopTime > gpGlobals->curtime )
		m_flSpeed = UTIL_Approach( 0, m_flSpeed, m_deceleration * gpGlobals->frametime );
		else
		m_flSpeed = UTIL_Approach( m_targetSpeed, m_flSpeed, m_acceleration * gpGlobals->frametime );

	float fraction = 2 * gpGlobals->frametime;
	SetAbsVelocity( ((m_vecMoveDir * m_flSpeed) * fraction) + (GetAbsVelocity() * (1-fraction)) );
}