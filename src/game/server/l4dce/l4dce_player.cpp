#include "cbase.h"
#include "l4dce_player.h"
#include "l4dce_playeranimstate.h"
#include "activitylist.h"
#include "datacache/imdlcache.h"
#include "in_buttons.h"
#include "trains.h"
#include "l4dce_viewmodel.h"
#include "l4dce_navarea.h"
#include "nav_pathfind.h"
#include "weapon_terror_base.h"
#include "l4dce_spawnpoint.h"
#include "tier0/memdbgon.h"

#define MAX_HEALTH 100

class CTEPlayerAnimEvent : public CBaseTempEntity
{
	public:
		DECLARE_CLASS(CTEPlayerAnimEvent, CBaseTempEntity);
		DECLARE_SERVERCLASS();

		CTEPlayerAnimEvent(const char *name)
			: CBaseTempEntity(name)
		{
			m_iPlayerIndex = -1;
		}

	public:
		CNetworkVar(int, m_iPlayerIndex);
		CNetworkVar(int, m_iEvent);
		CNetworkVar(int, m_nData);
};

IMPLEMENT_SERVERCLASS_ST_NOBASE(CTEPlayerAnimEvent, DT_TEPlayerAnimEvent)
	SendPropInt(SENDINFO(m_iPlayerIndex), 7, SPROP_UNSIGNED),
	SendPropInt(SENDINFO(m_iEvent), Q_log2(PLAYERANIMEVENT_COUNT) + 1, SPROP_UNSIGNED),
	SendPropInt(SENDINFO(m_nData), ANIMATION_SEQUENCE_BITS),
END_SEND_TABLE();

static CTEPlayerAnimEvent g_TEPlayerAnimEvent( "PlayerAnimEvent" );

void TE_PlayerAnimEvent(CBasePlayer *pPlayer, PlayerAnimEvent_t event, int nData)
{
	CPVSFilter filter(pPlayer->EyePosition());
	if(!IsCustomPlayerAnimEvent(event) && (event != PLAYERANIMEVENT_SNAP_YAW) && (event != PLAYERANIMEVENT_VOICE_COMMAND_GESTURE))
		filter.RemoveRecipient(pPlayer);

	Assert(pPlayer->entindex() >= 1 && pPlayer->entindex() <= MAX_PLAYERS);

	g_TEPlayerAnimEvent.m_iPlayerIndex = pPlayer->entindex();
	g_TEPlayerAnimEvent.m_iEvent = event;

	Assert(nData < (1 << ANIMATION_SEQUENCE_BITS));
	Assert((1 << ANIMATION_SEQUENCE_BITS) >= ActivityList_HighestIndex());

	g_TEPlayerAnimEvent.m_nData = nData;
	g_TEPlayerAnimEvent.Create(filter, 0);
}

LINK_ENTITY_TO_CLASS(player, CTerrorPlayer);
PRECACHE_REGISTER(player);

IMPLEMENT_SERVERCLASS(CTerrorPlayer, DT_TerrorPlayer);

BEGIN_SEND_TABLE_NOBASE(CTerrorPlayer, DT_TerrorLocalPlayerExclusive)
	SendPropVector(SENDINFO(m_vecOrigin), -1, SPROP_NOSCALE|SPROP_CHANGES_OFTEN, 0.0f, HIGH_DEFAULT, SendProxy_Origin),
	SendPropFloat(SENDINFO_VECTORELEM(m_angEyeAngles, 0), 8, SPROP_CHANGES_OFTEN, -90.0f, 90.0f),
END_SEND_TABLE()

BEGIN_SEND_TABLE_NOBASE(CTerrorPlayer, DT_TerrorNonLocalPlayerExclusive)
	SendPropVector(SENDINFO(m_vecOrigin), -1, SPROP_COORD_MP_LOWPRECISION|SPROP_CHANGES_OFTEN, 0.0f, HIGH_DEFAULT, SendProxy_Origin),
	SendPropFloat(SENDINFO_VECTORELEM(m_angEyeAngles, 0), 8, SPROP_CHANGES_OFTEN, -90.0f, 90.0f),
	SendPropAngle(SENDINFO_VECTORELEM(m_angEyeAngles, 1), 10, SPROP_CHANGES_OFTEN),
END_SEND_TABLE()

BEGIN_SEND_TABLE(CTerrorPlayer, DT_TerrorPlayer)
	SendPropExclude("DT_BaseEntity", "m_angRotation"),
	SendPropExclude("DT_BaseAnimating", "m_flPoseParameter"),
	SendPropExclude("DT_BaseAnimating", "m_flPlaybackRate"),
	SendPropExclude("DT_BaseAnimating", "m_nSequence"),
	SendPropExclude("DT_BaseAnimatingOverlay", "overlay_vars"),
	SendPropExclude("DT_BaseAnimating", "m_nNewSequenceParity"),
	SendPropExclude("DT_BaseAnimating", "m_nResetEventsParity"),
	SendPropExclude("DT_BaseEntity", "m_vecOrigin"),
	SendPropExclude("DT_ServerAnimationData" , "m_flCycle"),
	SendPropExclude("DT_AnimTimeMustBeFirst" , "m_flAnimTime"),
	SendPropDataTable("terror_localdata", 0, &REFERENCE_SEND_TABLE(DT_TerrorLocalPlayerExclusive), SendProxy_SendLocalDataTable),
	SendPropDataTable("terror_nonlocaldata", 0, &REFERENCE_SEND_TABLE(DT_TerrorNonLocalPlayerExclusive), SendProxy_SendNonLocalDataTable),
	SendPropInt(SENDINFO(m_nSurvivor), 8, SPROP_UNSIGNED),
END_SEND_TABLE();

BEGIN_DATADESC(CTerrorPlayer)
END_DATADESC();

BEGIN_PREDICTION_DATA(CTerrorPlayer)
END_PREDICTION_DATA();

CTerrorPlayer::CTerrorPlayer()
	: BaseClass()
{
	UseClientSideAnimation();

	m_angEyeAngles.Init();

	m_pAnimState = CreateTerrorPlayerAnimState(this);
}

CTerrorPlayer::~CTerrorPlayer()
{
	if(m_pAnimState) {
		m_pAnimState->ClearAnimationState();
		delete m_pAnimState;
	}
	m_pAnimState = nullptr;
}

CTerrorPlayer *CTerrorPlayer::CreatePlayer(const char *className, edict_t *ed)
{
	CTerrorPlayer::s_PlayerEdict = ed;
	CTerrorPlayer *player = static_cast<CTerrorPlayer *>(CreateEntityByName(className));
	return player;
}

CTerrorPlayer *ToTerrorPlayer(CBaseEntity *pEntity)
{
	if(!pEntity || !pEntity->IsPlayer())
		return nullptr;

#ifdef _DEBUG
	Assert(dynamic_cast<CTerrorPlayer *>(pEntity) != 0);
#endif

	return static_cast<CTerrorPlayer *>(pEntity);
}

int CTerrorPlayer::UpdateTransmitState()
{
	return SetTransmitState(FL_EDICT_ALWAYS);
}

void CTerrorPlayer::SetSurvivor(TerrorSurvivors survivor)
{
	ChangeTeam(TEAM_SURVIVOR);
	m_nSurvivor.GetForModify() = survivor;
	SetModel(GetModelForSurvivor(survivor));
	DoAnimationEvent(PLAYERANIMEVENT_SPAWN);
}

TerrorSurvivors CTerrorPlayer::GetSurvivor() const
{
	return m_nSurvivor;
}

void CTerrorPlayer::SetAnimation(PLAYER_ANIM playerAnim)
{
	switch(playerAnim)
	{
		case PLAYER_IDLE: {  break; }
		case PLAYER_WALK: {  break; }
		case PLAYER_JUMP: { DoAnimationEvent(PLAYERANIMEVENT_JUMP); break; }
		case PLAYER_SUPERJUMP: {  break; }
		case PLAYER_DIE: { DoAnimationEvent(PLAYERANIMEVENT_DIE); break; }
		case PLAYER_ATTACK1: { DoAnimationEvent(PLAYERANIMEVENT_ATTACK_PRIMARY); break; }
		case PLAYER_IN_VEHICLE: {  break; }
		case PLAYER_RELOAD: { DoAnimationEvent(PLAYERANIMEVENT_RELOAD); break; }
		case PLAYER_START_AIMING: {  break; }
		case PLAYER_LEAVE_AIMING: {  break; }
	}
}

void CTerrorPlayer::DoAnimationEvent(PlayerAnimEvent_t event, int nData)
{
	m_pAnimState->DoAnimationEvent(event, nData);
	TE_PlayerAnimEvent(this, event, nData);
}

void CTerrorPlayer::Precache()
{
	BaseClass::Precache();

	for(int i = FIRST_SURVIVOR; i <= LAST_SURVIVOR; i++) {
		PrecacheModel(GetModelForSurvivor((TerrorSurvivors)i));
		PrecacheModel(GetArmForSurvivor((TerrorSurvivors)i));
	}

	PrecacheScriptSound( "HL2Player.FlashLightOn" );
	PrecacheScriptSound( "HL2Player.FlashLightOff" );
}

void CTerrorPlayer::Spawn()
{
	g_bIsPlayerInSpawn = true;
	Msg("Player (%s) EntIndex: %d\n", GetPlayerName(), entindex());

	const char *szChosenSurvivor = engine->GetClientConVarValue(entindex(), "cl_prefered_survivor");
	int survivor = V_atoi(szChosenSurvivor);
	survivor = clamp(survivor, FIRST_SURVIVOR, LAST_SURVIVOR);
	SetSurvivor((TerrorSurvivors)survivor);

	BaseClass::Spawn();

	EquipSuit(false);
	SetHealth(MAX_HEALTH);
	m_takedamage = DAMAGE_YES;
	SetBloodColor(BLOOD_COLOR_RED);
	m_Local.m_iHideHUD = 0;
	SetImpactEnergyScale(4.0f);

	StopObserverMode();
	SetMaxSpeed(150.0f);
	SetMoveType(MOVETYPE_WALK);
	AddFlag(FL_ONGROUND);
	RemoveFlag(FL_FROZEN);
	SetPlayerUnderwater(false);
	m_Local.m_bInDuckJump = false;
	m_Local.m_bDucking = false;
	m_Local.m_bDucked = false;
	pl.deadflag = false;
	AddFlag(FL_CLIENT);
	SetFlashlightEnabled(true);

	SetCollisionGroup(COLLISION_GROUP_PLAYER);
	SetSolid(SOLID_BBOX);
	RemoveSolidFlags(FSOLID_NOT_SOLID);
	AddSolidFlags(FSOLID_NOT_STANDABLE);
	AddEFlags(EFL_DONTWALKON);

	RemoveEffects(EF_NODRAW);
	SetRenderFX(kRenderFxNone);
	SetRenderMode(kRenderNormal);
	HandleSpawnWeapons();

	g_bIsPlayerInSpawn = false;
}

ConVar spawnpistol("spawnpistol", "1", FCVAR_DEVELOPMENTONLY | FCVAR_CHEAT);

void CTerrorPlayer::HandleSpawnWeapons()
{
	if (spawnpistol.GetBool() && (GetTeamNumber() == TEAM_SURVIVOR))
		GiveNamedItem("weapon_pistol");
	
	GiveNamedItem("weapon_claw");
}

CStudioHdr *CTerrorPlayer::OnNewModel()
{
	CStudioHdr *hdr = BaseClass::OnNewModel();
	m_pAnimState->OnNewModel();
	return hdr;
}

void CTerrorPlayer::PostThink()
{
	BaseClass::PostThink();

	m_angEyeAngles = EyeAngles();

	QAngle angles = GetLocalAngles();
	angles[PITCH] = 0.0f;
	SetLocalAngles(angles);

	m_pAnimState->Update(m_angEyeAngles[YAW], m_angEyeAngles[PITCH]);
}

extern CBaseEntity	*g_pLastSpawn;
extern CBaseEntity *FindPlayerStart(const char *pszClassName);

CBaseEntity *CTerrorPlayer::EntSelectSpawnPoint()
{
	CBaseEntity *pSpot;
	edict_t		*player;

	player = edict();

// choose a info_player_deathmatch point
	pSpot = gEntList.FindEntityByClassname( NULL, "info_survivor_position");
	while ( pSpot )
	{
		if ( CSurvivorPosition *pSurvivorSpot = (CSurvivorPosition *)pSpot )
		{
			if ( GetSurvivorIndex() != pSurvivorSpot->GetSurvivorIndex() )
			{
				pSpot = gEntList.FindEntityByClassname( pSpot, "info_survivor_position");
				continue;
			}

			goto ReturnSpot;
		}
	}

	// If startspot is set, (re)spawn there.
	if ( !gpGlobals->startspot || !strlen(STRING(gpGlobals->startspot)))
	{
		pSpot = FindPlayerStart( "info_player_start" );
		if ( pSpot )
			goto ReturnSpot;
	}
	else
	{
		pSpot = gEntList.FindEntityByName( NULL, gpGlobals->startspot );
		if ( pSpot )
			goto ReturnSpot;
	}

ReturnSpot:
	if ( !pSpot  )
	{
		Warning( "PutClientInServer: no info_player_start on level\n");
		return CBaseEntity::Instance( INDEXENT( 0 ) );
	}

	g_pLastSpawn = pSpot;
	return pSpot;
}

extern int gEvilImpulse101;

void CTerrorPlayer::CheatImpulseCommands(int iImpulse)
{
	if (!sv_cheats->GetBool())
	{
		return;
	}

	switch (iImpulse)
	{
	case 101:
		gEvilImpulse101 = true;

		EquipSuit(false);
		SetHealth(MAX_HEALTH);

		GiveAmmo(999, "AMMO_TYPE_PISTOL", true);
		GiveAmmo(999, "AMMO_TYPE_PISTOL_MAGNUM", true);
		GiveAmmo(999, "AMMO_TYPE_ASSAULTRIFLE", true);
		GiveAmmo(999, "AMMO_TYPE_MINIGUN", true);
		GiveAmmo(999, "AMMO_TYPE_SMG", true);
		GiveAmmo(999, "AMMO_TYPE_M60", true);
		GiveAmmo(999, "AMMO_TYPE_SHOTGUN", true);
		GiveAmmo(999, "AMMO_TYPE_AUTOSHOTGUN", true);
		GiveAmmo(999, "AMMO_TYPE_HUNTINGRIFLE", true);
		GiveAmmo(999, "AMMO_TYPE_SNIPERRIFLE", true);
		GiveAmmo(999, "AMMO_TYPE_TURRET", true);
		GiveAmmo(999, "AMMO_TYPE_PIPEBOMB", true);
		GiveAmmo(999, "AMMO_TYPE_MOLOTOV", true);
		GiveAmmo(999, "AMMO_TYPE_VOMITJAR", true);
		GiveAmmo(999, "AMMO_TYPE_PAINPILLS", true);
		GiveAmmo(999, "AMMO_TYPE_FIRSTAID", true);
		GiveAmmo(999, "AMMO_TYPE_GRENADELAUNCHER", true);
		GiveAmmo(999, "AMMO_TYPE_ADRENALINE", true);
		GiveAmmo(999, "AMMO_TYPE_CHAINSAW", true);
		GiveAmmo(999, "AMMO_CARRIED_ITEM", true);

		gEvilImpulse101 = false;

		return;
	}

	BaseClass::CheatImpulseCommands(iImpulse);
}

void CTerrorPlayer::Weapon_Equip(CBaseCombatWeapon *pWeapon)
{
	if ( pWeapon->GetSlot() == WEAPON_SLOT_PRIMARY )
	{
		Weapon_DropSlot( WEAPON_SLOT_PRIMARY );
	}
	if ( pWeapon->GetSlot() == WEAPON_SLOT_SECONDARY )
	{
		Weapon_DropSlot( WEAPON_SLOT_SECONDARY );
	}
	if ( pWeapon->GetSlot() == WEAPON_SLOT_THROWABLE )
	{
		Weapon_DropSlot( WEAPON_SLOT_THROWABLE );
	}
	if ( pWeapon->GetSlot() == WEAPON_SLOT_DEPLOYABLE )
	{
		Weapon_DropSlot( WEAPON_SLOT_DEPLOYABLE );
	}
	if ( pWeapon->GetSlot() == WEAPON_SLOT_USEABLE )
	{
		Weapon_DropSlot( WEAPON_SLOT_USEABLE );
	}

	BaseClass::Weapon_Equip( pWeapon );
}

//-----------------------------------------------------------------------------
// Purpose: Player reacts to bumping a weapon. 
// Input  : pWeapon - the weapon that the player bumped into.
// Output : Returns true if player picked up the weapon
//-----------------------------------------------------------------------------
bool CTerrorPlayer::BumpWeapon(CBaseCombatWeapon *pWeapon)
{

	CBaseCombatCharacter *pOwner = pWeapon->GetOwner();

	// Can I have this weapon type?
	if ( pOwner || !Weapon_CanUse( pWeapon ) || !g_pGameRules->CanHavePlayerItem( this, pWeapon ) )
	{
		if ( gEvilImpulse101 )
		{
			UTIL_Remove( pWeapon );
		}
		return false;
	}

	// ----------------------------------------
	// If I already have it just take the ammo
	// ----------------------------------------
	if (Weapon_OwnsThisType( pWeapon->GetClassname(), pWeapon->GetSubType())) 
	{
		//Only remove the weapon if we attained ammo from it
		if ( Weapon_EquipAmmoOnly( pWeapon ) == false )
			return false;

		// Only remove me if I have no ammo left
		// Can't just check HasAnyAmmo because if I don't use clips, I want to be removed, 
		if ( pWeapon->UsesClipsForAmmo1() && pWeapon->HasPrimaryAmmo() )
			return false;

		UTIL_Remove( pWeapon );
		return false;
	}
	// -------------------------
	// Otherwise take the weapon
	// -------------------------
	else 
	{
		//Make sure we're not trying to take a new weapon type we already have
		if ( Weapon_SlotOccupied( pWeapon ) )
		{
			CBaseCombatWeapon *pActiveWeapon = Weapon_GetSlot( WEAPON_SLOT_PRIMARY );

			if ( pActiveWeapon != NULL && pActiveWeapon->HasAnyAmmo() == false && Weapon_CanSwitchTo( pWeapon ) )
			{
				Weapon_Equip( pWeapon );
				return true;
			}

			pActiveWeapon = Weapon_GetSlot( WEAPON_SLOT_SECONDARY );

			if ( pActiveWeapon != NULL && pActiveWeapon->HasAnyAmmo() == false && Weapon_CanSwitchTo( pWeapon ) )
			{
				Weapon_Equip( pWeapon );
				return true;
			}

			pActiveWeapon = Weapon_GetSlot( WEAPON_SLOT_THROWABLE );

			if ( pActiveWeapon != NULL && pActiveWeapon->HasAnyAmmo() == false && Weapon_CanSwitchTo( pWeapon ) )
			{
				Weapon_Equip( pWeapon );
				return true;
			}

			pActiveWeapon = Weapon_GetSlot( WEAPON_SLOT_DEPLOYABLE );

			if ( pActiveWeapon != NULL && pActiveWeapon->HasAnyAmmo() == false && Weapon_CanSwitchTo( pWeapon ) )
			{
				Weapon_Equip( pWeapon );
				return true;
			}

			pActiveWeapon = Weapon_GetSlot( WEAPON_SLOT_USEABLE );

			if ( pActiveWeapon != NULL && pActiveWeapon->HasAnyAmmo() == false && Weapon_CanSwitchTo( pWeapon ) )
			{
				Weapon_Equip( pWeapon );
				return true;
			}

			//Attempt to take ammo if this is the gun we're holding already
			if ( Weapon_OwnsThisType( pWeapon->GetClassname(), pWeapon->GetSubType() ) )
			{
				Weapon_EquipAmmoOnly( pWeapon );
			}

			return false;
		}

		pWeapon->CheckRespawn();

		pWeapon->AddSolidFlags( FSOLID_NOT_SOLID );
		pWeapon->AddEffects( EF_NODRAW );

		Weapon_Equip( pWeapon );

		EmitSound( "HL2Player.PickupWeapon" );
		
		return true;
	}

	//If we got here something fucked up happened so just do what the BasePlayer does
	return BaseClass::BumpWeapon( pWeapon );

}

//-----------------------------------------------------------------------------
// Purpose: 
// Input  : *cmd - 
// Output : Returns true on success, false on failure.
//-----------------------------------------------------------------------------
bool CTerrorPlayer::ClientCommand(const CCommand &args)
{

	//Drop primary weapon
	if ( !Q_stricmp( args[0], "DropPrimary" ) )
	{
		Weapon_DropSlot( WEAPON_SLOT_PRIMARY );
		return true;
	}

	if ( !Q_stricmp( args[0], "emit" ) )
	{
		CSingleUserRecipientFilter filter( this );
		if ( args.ArgC() > 1 )
		{
			EmitSound( filter, entindex(), args[ 1 ] );
		}
		else
		{
			EmitSound( filter, entindex(), "Test.Sound" );
		}
		return true;
	}

	return BaseClass::ClientCommand( args );
}

//-----------------------------------------------------------------------------
// Purpose: 
// Output : void CBasePlayer::PlayerUse
//-----------------------------------------------------------------------------
void CTerrorPlayer::PlayerUse ( void )
{
	// Was use pressed or released?
	if ( ! ((m_nButtons | m_afButtonPressed | m_afButtonReleased) & IN_USE) )
		return;

	if ( m_afButtonPressed & IN_USE )
	{
		// Currently using a latched entity?
		if ( ClearUseEntity() )
		{
			return;
		}
		else
		{
			if ( m_afPhysicsFlags & PFLAG_DIROVERRIDE )
			{
				m_afPhysicsFlags &= ~PFLAG_DIROVERRIDE;
				m_iTrain = TRAIN_NEW|TRAIN_OFF;
				return;
			}
			else
			{	// Start controlling the train!
				CBaseEntity *pTrain = GetGroundEntity();
				if ( pTrain && !(m_nButtons & IN_JUMP) && (GetFlags() & FL_ONGROUND) && (pTrain->ObjectCaps() & FCAP_DIRECTIONAL_USE) && pTrain->OnControls(this) )
				{
					m_afPhysicsFlags |= PFLAG_DIROVERRIDE;
					m_iTrain = TrainSpeed(pTrain->m_flSpeed, ((CFuncTrackTrain*)pTrain)->GetMaxSpeed());
					m_iTrain |= TRAIN_NEW;
					EmitSound( "HL2Player.TrainUse" );
					return;
				}
			}
		}

		// Tracker 3926:  We can't +USE something if we're climbing a ladder
		if ( GetMoveType() == MOVETYPE_LADDER )
		{
			return;
		}
	}

	CBaseEntity *pUseEntity = FindUseEntity();

	bool usedSomething = false;

	// Found an object
	if ( pUseEntity )
	{
		//!!!UNDONE: traceline here to prevent +USEing buttons through walls			
		int caps = pUseEntity->ObjectCaps();
		variant_t emptyVariant;

		if ( m_afButtonPressed & IN_USE )
		{
			// Robin: Don't play sounds for NPCs, because NPCs will allow respond with speech.
			if ( !pUseEntity->MyNPCPointer() )
			{
				EmitSound( "HL2Player.Use" );
			}
		}

		if ( ( (m_nButtons & IN_USE) && (caps & FCAP_CONTINUOUS_USE) ) ||
			 ( (m_afButtonPressed & IN_USE) && (caps & (FCAP_IMPULSE_USE|FCAP_ONOFF_USE)) ) )
		{
			if ( caps & FCAP_CONTINUOUS_USE )
				m_afPhysicsFlags |= PFLAG_USING;

			pUseEntity->AcceptInput( "Use", this, this, emptyVariant, USE_TOGGLE );

			usedSomething = true;
		}
		// UNDONE: Send different USE codes for ON/OFF.  Cache last ONOFF_USE object to send 'off' if you turn away
		else if ( (m_afButtonReleased & IN_USE) && (pUseEntity->ObjectCaps() & FCAP_ONOFF_USE) )	// BUGBUG This is an "off" use
		{
			pUseEntity->AcceptInput( "Use", this, this, emptyVariant, USE_TOGGLE );

			usedSomething = true;
		}


		//Check for weapon pick-up
		if ( m_afButtonPressed & IN_USE )
		{
			CBaseCombatWeapon *pWeapon = dynamic_cast<CBaseCombatWeapon *>(pUseEntity);

			if ( ( pWeapon != NULL ) && ( Weapon_CanSwitchTo( pWeapon ) ) )
			{
				//Try to take ammo or swap the weapon
				if ( Weapon_OwnsThisType( pWeapon->GetClassname(), pWeapon->GetSubType() ) )
				{
					Weapon_EquipAmmoOnly( pWeapon );
				}
				else
				{
					Weapon_DropSlot( pWeapon->GetSlot() );
					Weapon_Equip( pWeapon );
				}

				usedSomething = true;
			}
		}
	}
	else if ( m_afButtonPressed & IN_USE )
	{
		// Signal that we want to play the deny sound, unless the user is +USEing on a ladder!
		// The sound is emitted in ItemPostFrame, since that occurs after GameMovement::ProcessMove which
		// lets the ladder code unset this flag.
		m_bPlayUseDenySound = true;
	}

	// Debounce the use key
	if ( usedSomething && pUseEntity )
	{
		m_Local.m_nOldButtons |= IN_USE;
		m_afButtonPressed &= ~IN_USE;
	}
}

void CTerrorPlayer::CreateViewModel(int iViewModel)
{
	Assert((iViewModel >= 0) && (iViewModel < MAX_VIEWMODELS));


    if ( GetViewModel( VMINDEX_WEP ) != nullptr )
    {
        if ( GetViewModel( VMINDEX_HANDS ) == nullptr )
        {
            Warning( "Weapon viewmodel exists but hands don't!!\n" );
        }

        return;
    }


	CTerrorViewModel* vm = static_cast<CTerrorViewModel*>(CreateEntityByName("terror_viewmodel"));
    
    if ( vm )
    {
        vm->SetAbsOrigin( GetAbsOrigin() );
        vm->SetOwner( this );
        vm->SetIndex( VMINDEX_WEP );
        DispatchSpawn( vm );
        vm->FollowEntity( this, false );
        m_hViewModel.Set( VMINDEX_WEP, vm );


		CTerrorViewModel* vmhands = static_cast<CTerrorViewModel*>(CreateEntityByName("terror_viewmodel"));
    
        if ( vmhands )
        {
            vmhands->SetAbsOrigin( GetAbsOrigin() );
            vmhands->SetOwner( this );
            vmhands->SetIndex( VMINDEX_HANDS );
            DispatchSpawn( vmhands );
            vmhands->FollowEntity( vm, true ); // Sets moveparent.
            m_hViewModel.Set( VMINDEX_HANDS, vmhands );
        }
    }

    SetHandsModel( nullptr );
}

int CTerrorPlayer::FlashlightIsOn()
{
	return IsEffectActive(EF_DIMLIGHT);
}

bool CTerrorPlayer::FlashlightTurnOn(bool playSound)
{
	if(!m_bFlashLightEnabled)
		return false;

	AddEffects(EF_DIMLIGHT);

	if(playSound)
		EmitSound("HL2Player.FlashLightOn");

	return true;
}

void CTerrorPlayer::FlashlightTurnOff(bool playSound)
{
	RemoveEffects(EF_DIMLIGHT);

	if(playSound)
		EmitSound("HL2Player.FlashLightOff");
}


CTerrorViewModel* CTerrorPlayer::GetViewModel( int index ) const
{
	return static_cast<CTerrorViewModel*>(m_hViewModel[index].Get());
}

void CTerrorPlayer::SetHandsModel( const char* model )
{
    CPredictedViewModel* vmhands = GetViewModel( VMINDEX_HANDS );

    if ( !vmhands ) return;


	vmhands->SetWeaponModel(TranslatePlayerModelToHands(), nullptr);
}

const char* CTerrorPlayer::TranslatePlayerModelToHands()
{
	const char *szChosenSurvivor = engine->GetClientConVarValue(entindex(), "cl_prefered_survivor");
	int survivor = V_atoi(szChosenSurvivor);
	survivor = clamp(survivor, FIRST_SURVIVOR, LAST_SURVIVOR);

	return GetArmForSurvivor(TerrorSurvivors(survivor));
}

void CTerrorPlayer::Event_Killed(const CTakeDamageInfo &info)
{
	SetHandsModel("models/error.mdl");
	BaseClass::Event_Killed(info);
}

CTerrorWeaponBase *CTerrorPlayer::GetActiveTerrorWeapon() const
{
	return dynamic_cast< CTerrorWeaponBase* >(GetActiveWeapon());
}

#ifdef DEBUG
CON_COMMAND(survivor, "")
{
	CTerrorPlayer *player = (CTerrorPlayer *)UTIL_GetCommandClient();
	TerrorSurvivors survivor = (TerrorSurvivors)clamp(V_atoi(args.Arg(1)), FIRST_SURVIVOR, LAST_SURVIVOR);
	player->SetSurvivor(survivor);
}
#endif