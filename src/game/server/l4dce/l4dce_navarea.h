#ifndef _L4DCE_NAVAREA_H_
#define _L4DCE_NAVAREA_H_

#pragma once

#include "nav_area.h"

class CTerrorPlayer;

class TerrorNavArea : public CNavArea
{
	private:
		DECLARE_CLASS_GAMEROOT(TerrorNavArea, CNavArea);

	public:
		enum
		{
			NAV_PLAYERCLIP = NAV_MESH_FIRST_CUSTOM,
			NAV_BREAKABLEWALL,
		};

		//SpawnAttributes
		enum
		{
			EMPTY = (1 << 1),
			STOP = (1 << 2),
			FINALE = (1 << 6),
			PLAYER_START = (1 << 7),
			BATTLEFIELD = (1 << 8),
			IGNORE_VISIBILITY = (1 << 9),
			NOT_CLEARABLE = (1 << 10),
			CHECKPOINT = (1 << 11),
			OBSCURED = (1 << 12),
			NO_MOBS = (1 << 13),
			THREAT = (1 << 14),
			RESCUE_VEHICLE = (1 << 15),
			RESCUE_CLOSET = (1 << 16),

			//BATTLESTATION = ???,
			//ESCAPE_ROUTE = ???,
			//DOOR = ???,
		};

		virtual void Save(CUtlBuffer &fileBuffer, unsigned int version) const;
		virtual NavErrorType Load(CUtlBuffer &fileBuffer, unsigned int version, unsigned int subVersion);

		void SetSpawnAttributes(unsigned int attributes);
		void RemoveSpawnAttributes(unsigned int attributes);
		unsigned int GetSpawnAttributes() const;

	private:
		unsigned int m_SpawnAttributes = 0;
};

#endif