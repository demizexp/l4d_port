class CInfoDirector : public CBaseEntity
{
public:
	DECLARE_CLASS( CInfoDirector, CBaseEntity );
	DECLARE_DATADESC();

	void InputPanicEvent( inputdata_t &inputdata );
	void InputPanicEventControlled( inputdata_t &inputdata );
	void InputForceSurvivorPositions( inputdata_t &inputdata );
	void InputReleaseSurvivorPositions( inputdata_t &inputdata );
	void InputFireConceptToAny( inputdata_t &inputdata );
	void InputStartIntro( inputdata_t &inputdata );
	void InputFinishIntro( inputdata_t &inputdata );
	void InputEnableTankFrustration( inputdata_t &inputdata );
	void InputDisableTankFrustration( inputdata_t &inputdata );

	COutputEvent m_OnGamePlayStart;
	COutputEvent m_OnPanicEventFinished;
};