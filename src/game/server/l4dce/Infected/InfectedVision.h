#ifndef _INFECTED_VISION_H_
#define _INFECTED_VISION_H_

#pragma once

#include "NextBotVisionInterface.h"

class Infected;

class InfectedVision : public IVision
{
	private:
		DECLARE_CLASS_GAMEROOT(InfectedVision, IVision);

	public:
		InfectedVision(Infected *infected);

		virtual void OnContact(CBaseEntity *other, CGameTrace *result = nullptr) override;
		virtual void OnInjured(const CTakeDamageInfo &info) override;
		virtual float GetMinRecognizeTime() const override;
		virtual float GetDefaultMaxVisionRange() const override;
		virtual float GetDefaultFieldOfView() const;

	private:
		Infected *m_pInfected = nullptr;
};

#endif