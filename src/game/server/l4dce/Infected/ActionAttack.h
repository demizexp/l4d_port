#ifndef _ACTION_ATTACK_H_
#define _ACTION_ATTACK_H_

#pragma once

#include "NextBotBehavior.h"
#include "Path/NextBotChasePath.h"
#include "ActionShared.h"

class Infected;
class InfectedPathCost;

class InfectedAlert : public Action<Infected>
{
	public:
		enum AlertType {
			SIGHT,
			CONTACT,
			INJURED,
		};

		InfectedAlert(CBaseEntity *alerter, AlertType type);
		virtual const char *GetName() const override { return "InfectedAlert"; }
		virtual ActionResult<Infected> OnStart(Infected *me, Action<Infected> *priorAction) override;
		virtual ActionResult<Infected> OnResume(Infected *me, Action<Infected> *interruptingAction) override;
		virtual ActionResult<Infected> Update(Infected *me, float interval) override;
		virtual EventDesiredResult<Infected> OnAnimationActivityComplete(Infected *me, int activity) override;
		virtual EventDesiredResult<Infected> OnSight(Infected *me, CBaseEntity *subject) override;

		bool LookAtDisturbance(Infected *me);

	private:
		AlertType m_nAlertType = SIGHT;
		CBaseEntity *m_pTarget = nullptr;
		CBaseEntity *m_pAlerter = nullptr;
		CountdownTimer m_BlindTimer;
};

class InfectedAttack : public Action<Infected>
{
	public:
		InfectedAttack(CBaseEntity *target);
		virtual const char *GetName() const override { return "InfectedAttack"; }
		virtual ActionResult<Infected> OnStart(Infected *me, Action<Infected> *priorAction) override;
		virtual Action<Infected> *InitialContainedAction(Infected *me) override;
		virtual EventDesiredResult<Infected> OnAnimationEvent(Infected *me, animevent_t *event) override;
		virtual ActionResult<Infected> Update(Infected *me, float interval) override;

		CBaseEntity *GetTarget() const { return m_pTarget; }

	private:
		CBaseEntity *m_pTarget = nullptr;
};

class InfectedChase : public Action<Infected>
{
	public:
		virtual const char *GetName() const override { return "InfectedChase"; }
		virtual ActionResult<Infected> OnStart(Infected *me, Action<Infected> *priorAction) override;
		virtual ActionResult<Infected> Update(Infected *me, float interval) override;
		virtual ActionResult<Infected> OnResume(Infected *me, Action<Infected> *interruptingAction) override;

		InfectedAttack *GetAttack() const { return static_cast<InfectedAttack *>(GetParentAction()); }
		CBaseEntity *GetTarget() const { return GetAttack()->GetTarget(); }

	private:
		InfectedPathCost m_PathCost;
		DirectChasePath m_ChasePath;
};

class InfectedPunch : public Action<Infected>
{
	public:
		virtual const char *GetName() const override { return "InfectedPunch"; }
		virtual ActionResult<Infected> OnStart(Infected *me, Action<Infected> *priorAction) override;
		virtual ActionResult<Infected> Update(Infected *me, float interval) override;

		InfectedAttack *GetAttack() const { return static_cast<InfectedAttack *>(GetParentAction()); }
		CBaseEntity *GetTarget() const { return GetAttack()->GetTarget(); }
};

#endif