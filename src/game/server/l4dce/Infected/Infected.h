#ifndef _INFECTED_H_INCLUDED_
#define _INFECTED_H_INCLUDED_

#pragma once

#include "NextBot.h"
#include "NextBotBehavior.h"

extern Activity ACT_EXP_ANGRY;
extern Activity ACT_EXP_IDLE;

class InfectedLocomotion;
class InfectedBody;
class InfectedVision;

class Infected : public NextBotCombatCharacter
{
	private:
		DECLARE_CLASS(Infected, NextBotCombatCharacter);

	public:
		Infected();
		virtual ~Infected() override;

		virtual void Precache() override;
		virtual void Spawn() override;

		virtual Class_T Classify() override { return CLASS_ZOMBIE; }
		virtual ILocomotion *GetLocomotionInterface() const override;
		virtual IBody *GetBodyInterface() const override;
		virtual IVision *GetVisionInterface() const override;

		DECLARE_INTENTION_INTERFACE(Infected);

		virtual void OnModelChanged() override;
		virtual Vector EyePosition() override;
		virtual void Update() override;

		InfectedLocomotion *GetLocomotion() const { return m_pLocomotion; }
		InfectedBody *GetBody() const { return m_pBody; }
		InfectedVision *GetVision() const { return m_pVision; }
		Behavior<Infected> *GetBehavior() const;

		bool IsGroundLevel(float radius) const;

	private:
		InfectedLocomotion *m_pLocomotion = nullptr;
		InfectedBody *m_pBody = nullptr;
		InfectedVision *m_pVision = nullptr;

		int m_nForward = -1;

		Vector m_vecEye;
		QAngle m_angEye;
};

#endif