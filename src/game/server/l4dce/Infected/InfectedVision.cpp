#include "cbase.h"
#include "InfectedVision.h"
#include "Infected.h"
#include "tier0/memdbgon.h"

InfectedVision::InfectedVision(Infected *infected)
	: BaseClass(infected), m_pInfected(infected)
{
	
}

void InfectedVision::OnContact(CBaseEntity *other, CGameTrace *result)
{
	BaseClass::OnContact(other, result);
}

void InfectedVision::OnInjured(const CTakeDamageInfo &info)
{
	BaseClass::OnInjured(info);
}

float InfectedVision::GetDefaultMaxVisionRange() const
{
	return 2000.0f;
}

float InfectedVision::GetDefaultFieldOfView() const
{
	return 90.0f;
}

float InfectedVision::GetMinRecognizeTime() const
{
	return 0.0f;
}