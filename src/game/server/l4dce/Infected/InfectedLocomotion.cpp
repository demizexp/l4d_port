#include "cbase.h"
#include "InfectedLocomotion.h"
#include "Infected.h"
#include "NextBotUtil.h"
#include "tier0/memdbgon.h"

InfectedLocomotion::InfectedLocomotion(Infected *infected)
	: BaseClass(infected), m_pInfected(infected)
{
	
}

float InfectedLocomotion::GetRunSpeed() const
{
	return 250.0f;
}

float InfectedLocomotion::GetWalkSpeed() const
{
	return 75.0f;
}

float InfectedLocomotion::GetMaxAcceleration() const
{
	return 200.0f;
}

float InfectedLocomotion::GetMaxDeceleration() const
{
	return 350.0f;
}

float InfectedLocomotion::GetMaxYawRate() const
{
	return 250.0f;
}

bool InfectedLocomotion::ClimbUpToLedge(const Vector &landingGoal, const Vector &landingForward, const CBaseEntity *obstacle)
{
	if(!IsOnGround())
		return false;

	const Vector &feet = GetFeet();

	Activity idealActivity = ACT_INVALID;

	float height = (landingGoal.z - feet.z);
	if(height >= 24.0f)
		idealActivity = ACT_TERROR_CLIMB_24_FROM_STAND;
	if(height >= 36.0f)
		idealActivity = ACT_TERROR_CLIMB_36_FROM_STAND;
	if(height >= 48.0f)
		idealActivity = ACT_TERROR_CLIMB_48_FROM_STAND;
	if(height >= 60.0f)
		idealActivity = ACT_TERROR_CLIMB_60_FROM_STAND;
	if(height >= 72.0f)
		idealActivity = ACT_TERROR_CLIMB_72_FROM_STAND;
	if(height >= 84.0f)
		idealActivity = ACT_TERROR_CLIMB_84_FROM_STAND;
	if(height >= 96.0f)
		idealActivity = ACT_TERROR_CLIMB_96_FROM_STAND;
	if(height >= 108.0f)
		idealActivity = ACT_TERROR_CLIMB_108_FROM_STAND;
	if(height >= 120.0f)
		idealActivity = ACT_TERROR_CLIMB_120_FROM_STAND;
	if(height >= 132.0f)
		idealActivity = ACT_TERROR_CLIMB_132_FROM_STAND;
	if(height >= 144.0f)
		idealActivity = ACT_TERROR_CLIMB_144_FROM_STAND;
	if(height >= 156.0f)
		idealActivity = ACT_TERROR_CLIMB_156_FROM_STAND;
	if(height >= 168.0f)
		idealActivity = ACT_TERROR_CLIMB_168_FROM_STAND;

	if(idealActivity == ACT_INVALID)
		return false;

	#pragma message(FILE_LINE_STRING " find out how to implement climbing")

	if(m_pInfected->IsDebugging(NEXTBOT_LOCOMOTION)) {
		NDebugOverlay::Cross3D(feet, 5.0f, 0, 255, 0, true, 10.0f);
		NDebugOverlay::Cross3D(landingGoal, 5.0f, 0, 255, 0, true, 10.0f);
		NDebugOverlay::Line(feet, landingGoal, 0, 255, 0, true, 10.0f);
		NDebugOverlay::HorzArrow(landingGoal, (landingGoal + (landingForward * 10.0f)), 2.0f, 0, 255, 0, 255, true, 10.0f);
	}

	return false;
}