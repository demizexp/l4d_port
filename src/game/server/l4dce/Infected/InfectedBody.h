#ifndef _INFECTED_BODY_H_
#define _INFECTED_BODY_H_

#pragma once

#include "NextBotBodyInterface.h"

class Infected;

class InfectedBody : public IBody
{
	private:
		DECLARE_CLASS_GAMEROOT(InfectedBody, IBody);

	public:
		InfectedBody(Infected *infected);

		virtual bool StartActivity(Activity act, unsigned int flags = 0) override;
		virtual int SelectAnimationSequence(Activity act) const override;
		virtual Activity GetActivity() const override;
		virtual bool IsActivity(Activity act) const override;
		virtual bool HasActivityType(unsigned int flags) const override;
		virtual unsigned int GetSolidMask() const override;
		virtual unsigned int GetCollisionGroup() const override;
		virtual void Update() override;
		virtual void OnModelChanged() override;
		virtual void OnAnimationActivityComplete(int activity) override;
		virtual void OnAnimationActivityInterrupted(int activity) override;
		virtual void Reset() override;
		virtual void SetArousal(ArousalType arousal) override;
		virtual ArousalType GetArousal() const override;
		virtual bool IsArousal(ArousalType arousal) const override;
		virtual const Vector &GetViewVector() const;
		virtual void SetDesiredPosture(PostureType posture) override;
		virtual PostureType GetDesiredPosture() const override;
		virtual bool IsDesiredPosture(PostureType posture) const override;
		virtual bool IsInDesiredPosture() const override;
		virtual PostureType GetActualPosture() const override;
		virtual bool IsActualPosture(PostureType posture) const override;
		virtual bool IsPostureMobile() const override;
		virtual bool IsPostureChanging() const override;
		virtual void OnPostureChanged() override;

		virtual void AimHeadTowards(const Vector &lookAtPos, LookAtPriorityType priority = BORING, float duration = 0.0f, INextBotReply *replyWhenAimed = nullptr, const char *reason = nullptr) override;
		virtual void AimHeadTowards(CBaseEntity *subject, LookAtPriorityType priority = BORING, float duration = 0.0f, INextBotReply *replyWhenAimed = nullptr, const char *reason = nullptr) override;
		virtual bool IsHeadAimingOnTarget() const override;
		virtual bool IsHeadSteady() const override;
		virtual float GetHeadSteadyDuration() const override;
		//virtual float GetHeadAimSubjectLeadTime() const override;
		//virtual float GetHeadAimTrackingInterval() const override;
		virtual void ClearPendingAimReply() override;
		virtual float GetMaxHeadAngularVelocity() const override;

	private:
		Infected *m_pInfected = nullptr;

		Activity m_nOldDesiredActivity = ACT_INVALID;
		int m_nOldActivityFlags = 0;

		Activity m_nDesiredActivity = ACT_INVALID;
		int m_nActivityFlags = 0;

		ArousalType m_nArousal = NEUTRAL;

		PostureType m_nActualPosture = STAND;
		PostureType m_nDesiredPosture = STAND;
		bool m_bChangingPosture = false;
		Activity m_nPostureActvity = ACT_INVALID;

		Vector m_lookAtPos;
		EHANDLE m_lookAtSubject;
		Vector m_lookAtVelocity;
		CountdownTimer m_lookAtTrackingTimer;
		LookAtPriorityType m_lookAtPriority = BORING;
		CountdownTimer m_lookAtExpireTimer;
		IntervalTimer m_lookAtDurationTimer;
		INextBotReply *m_lookAtReplyWhenAimed = nullptr;
		bool m_isSightedIn = false;
		bool m_hasBeenSightedIn = false;
		IntervalTimer m_headSteadyTimer;
		CountdownTimer m_anchorRepositionTimer;
		Vector m_anchorForward;
		QAngle m_priorAngles;

		int m_nMoveX = -1;
		int m_nMoveY = -1;
		int m_nLeanYaw = -1;
		int m_nLeanPitch = -1;
		int m_nBodyYaw = -1;
		int m_nBodyPitch = -1;

		CUtlVector<int> m_BadSequences;
};

#endif