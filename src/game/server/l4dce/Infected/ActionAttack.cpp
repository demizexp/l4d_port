#include "cbase.h"
#include "Infected.h"
#include "ActionAttack.h"
#include "ActionWander.h"
#include "l4dce_shareddefs.h"
#include "tier0/memdbgon.h"

extern ConVar nb_blind;

InfectedAlert::InfectedAlert(CBaseEntity *alerter, AlertType type)
	: Action<Infected>(), m_pAlerter(alerter), m_nAlertType(type)
{

}

ActionResult<Infected> InfectedAlert::OnStart(Infected *me, Action<Infected> *priorAction)
{
	IBody *body = me->GetBodyInterface();

	body->SetArousal(IBody::ALERT);

	if(!body->IsPostureMobile())
		return SuspendFor(new InfectedChangePosture(IBody::STAND));

	OnResume(me, nullptr);
	return Continue();
}

ActionResult<Infected> InfectedAlert::OnResume(Infected *me, Action<Infected> *interruptingAction)
{
	if(!LookAtDisturbance(me))
		return ChangeTo(new InfectedWander());

	return Continue();
}

bool InfectedAlert::LookAtDisturbance(Infected *me)
{
	if(!m_pAlerter)
		return false;

	Activity idealActivity = ACT_INVALID;

	switch(DirectionBetweenEntities(me, m_pAlerter)) {
		default:
		case FORWARD: idealActivity = ACT_TERROR_IDLE_ALERT_AHEAD; break;
		case BACKWARD: idealActivity = ACT_TERROR_IDLE_ALERT_BEHIND; break;
		case RIGHT: idealActivity = ACT_TERROR_IDLE_ALERT_RIGHT; break;
		case LEFT: idealActivity = ACT_TERROR_IDLE_ALERT_LEFT; break;
	}

	if(m_nAlertType == INJURED) {
		switch(idealActivity) {
			default:
			case ACT_TERROR_IDLE_ALERT_AHEAD: idealActivity = ACT_TERROR_IDLE_ALERT_INJURED_AHEAD; break;
			case ACT_TERROR_IDLE_ALERT_BEHIND: idealActivity = ACT_TERROR_IDLE_ALERT_INJURED_BEHIND; break;
			case ACT_TERROR_IDLE_ALERT_LEFT: idealActivity = ACT_TERROR_IDLE_ALERT_INJURED_LEFT; break;
			case ACT_TERROR_IDLE_ALERT_RIGHT: idealActivity = ACT_TERROR_IDLE_ALERT_INJURED_RIGHT; break;
		}
	}

	if(idealActivity == ACT_INVALID)
		return false;
	
	if(!me->GetBodyInterface()->StartActivity(idealActivity, IBody::MOTION_CONTROLLED_XY))
		return false;

	if(m_nAlertType == INJURED)
		m_BlindTimer.Start(RandomFloat(1.0f, 2.0f));

	return true;
}

EventDesiredResult<Infected> InfectedAlert::OnAnimationActivityComplete(Infected *me, int activity)
{
	IBody *body = me->GetBodyInterface();

	if(activity == ACT_TERROR_NEUTRAL_TO_ALERT)
		body->StartActivity(ACT_TERROR_IDLE_ALERT, IBody::MOTION_CONTROLLED_XY);
	if(activity == ACT_TERROR_IDLE_ALERT)
		body->StartActivity(ACT_TERROR_ALERT_TO_NEUTRAL, IBody::MOTION_CONTROLLED_XY);
	else if(activity == ACT_TERROR_ALERT_TO_NEUTRAL)
		return TryChangeTo(new InfectedWander());
	else if(activity >= ACT_TERROR_IDLE_ALERT_INJURED_AHEAD && activity <= ACT_TERROR_IDLE_ALERT_RIGHT)
		body->StartActivity(ACT_TERROR_IDLE_ALERT, IBody::MOTION_CONTROLLED_XY);

	return TryContinue();
}

ActionResult<Infected> InfectedAlert::Update(Infected *me, float interval)
{
	if(m_BlindTimer.HasStarted() && m_BlindTimer.IsElapsed() && m_pTarget)
		return static_cast<ActionResult<Infected>>(OnSight(me, m_pTarget));

	if(m_pAlerter) {
		me->GetBodyInterface()->AimHeadTowards(m_pAlerter, IBody::IMPORTANT, 1.0f, nullptr);
		//me->GetLocomotionInterface()->FaceTowards(m_pAlerter->GetAbsOrigin());
	}

	return Continue();
}

EventDesiredResult<Infected> InfectedAlert::OnSight(Infected *me, CBaseEntity *subject)
{
	if(!subject->IsPlayer() || subject->GetTeamNumber() != TEAM_SURVIVOR)
		return TryContinue();

	m_pTarget = subject;

	if(!m_BlindTimer.HasStarted() || (m_BlindTimer.HasStarted() && m_BlindTimer.IsElapsed()))
		return TryChangeTo(new InfectedStandingActivity(ACT_TERROR_IDLE_ACQUIRE, RandomFloat(0.5f, 2.0f), new InfectedAttack(subject)));

	return TryContinue();
}

InfectedAttack::InfectedAttack(CBaseEntity *target)
	: Action<Infected>(), m_pTarget(target)
{

}

ActionResult<Infected> InfectedAttack::OnStart(Infected *me, Action<Infected> *priorAction)
{
	IBody *body = me->GetBodyInterface();

	body->SetArousal(IBody::INTENSE);

	if(!body->IsPostureMobile())
		return SuspendFor(new InfectedChangePosture(IBody::STAND));

	return Continue();
}

ActionResult<Infected> InfectedAttack::Update(Infected *me, float interval)
{
	const CKnownEntity *threat = me->GetVisionInterface()->GetPrimaryKnownThreat(true);

	if(nb_blind.GetBool()) {
		m_pTarget = nullptr;
		threat = nullptr;
	}

	if(!m_pTarget || !m_pTarget->IsAlive() && !threat) {
		if(me->GetLocomotionInterface()->IsRunning())
			return ChangeTo(new InfectedStandingActivity(ACT_TERROR_RUN_INTENSE_TO_STAND_ALERT, 0.0f, new InfectedAlert(nullptr, InfectedAlert::SIGHT)));
		else
			return ChangeTo(new InfectedAlert(nullptr, InfectedAlert::SIGHT));
	} else if(!m_pTarget || !m_pTarget->IsAlive() && threat && threat->GetEntity() != m_pTarget)
		m_pTarget = threat->GetEntity();

	me->GetBodyInterface()->AimHeadTowards(m_pTarget, IBody::IMPORTANT, 1.0f, nullptr);

	return Continue();
}

Action<Infected> *InfectedAttack::InitialContainedAction(Infected *me)
{
	return new InfectedChase();
}

ActionResult<Infected> InfectedChase::OnStart(Infected *me, Action<Infected> *priorAction)
{
	m_PathCost.SetInfected(me);
	m_ChasePath.SetChaseHow(ChasePath::LEAD_SUBJECT);
	m_ChasePath.SetGoalTolerance(25.0f);
	m_ChasePath.SetMinLookAheadDistance(me->GetVisionInterface()->GetMaxVisionRange());
	OnResume(me, nullptr);
	return Continue();
}

ActionResult<Infected> InfectedChase::OnResume(Infected *me, Action<Infected> *interruptingAction)
{
	me->GetBodyInterface()->StartActivity(ACT_TERROR_RUN_INTENSE);
	return Continue();
}

ActionResult<Infected> InfectedChase::Update(Infected *me, float interval)
{
	CBaseEntity *target = GetTarget();
	if(!target)
		return Done();

	const float hullwidth = me->GetBodyInterface()->GetHullWidth();
	const float distance = me->GetRangeTo(target);

	if(distance < hullwidth)
		return SuspendFor(new InfectedPunch());
	else if(distance < (hullwidth * 1.5f))
		me->AddGesture(ACT_TERROR_ATTACK);

	m_ChasePath.Update(me, GetTarget(), m_PathCost);
	return Continue();
}

ActionResult<Infected> InfectedPunch::OnStart(Infected *me, Action<Infected> *priorAction)
{
	me->RemoveGesture(ACT_TERROR_ATTACK);
	me->GetBodyInterface()->StartActivity(ACT_TERROR_ATTACK_CONTINUOUSLY);
	return Continue();
}

ActionResult<Infected> InfectedPunch::Update(Infected *me, float interval)
{
	CBaseEntity *target = GetTarget();
	if(!target)
		return Done();

	me->GetLocomotionInterface()->FaceTowards(target->GetAbsOrigin());

	if(me->GetRangeTo(target) > me->GetBodyInterface()->GetHullWidth())
		return Done();

	return Continue();
}

EventDesiredResult<Infected> InfectedAttack::OnAnimationEvent(Infected *me, animevent_t *event)
{
	IBody *body = me->GetBodyInterface();

	int i = event->Event();
	if(i == AE_ATTACK_HIT) 
		me->CheckTraceHullAttack(body->GetHullWidth(), body->GetHullMins(), body->GetHullMaxs(), 1.0f, DMG_SLASH | DMG_CLUB, 1.0f, false);

	return TryContinue();
}