#ifndef _INFECTED_LOCOMOTION_H_
#define _INFECTED_LOCOMOTION_H_

#pragma once

#include "NextBotGroundLocomotion.h"

class Infected;

class InfectedLocomotion : public NextBotGroundLocomotion
{
	private:
		DECLARE_CLASS_GAMEROOT(InfectedLocomotion, NextBotGroundLocomotion);

	public:
		InfectedLocomotion(Infected *infected);

		virtual float GetRunSpeed() const override;
		virtual float GetWalkSpeed() const override;
		virtual bool ClimbUpToLedge(const Vector &landingGoal, const Vector &landingForward, const CBaseEntity *obstacle) override;
		virtual float GetMaxYawRate() const override;
		virtual float GetMaxAcceleration() const override;
		virtual float GetMaxDeceleration() const override;

	private:
		Infected *m_pInfected = nullptr;
};

#endif