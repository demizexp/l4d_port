#ifndef _DIRECTOR_H_
#define _DIRECTOR_H_

#pragma once

#include "igamesystem.h"
#include "l4dce_navarea.h"

class CNavArea;
class CTerrorPlayer;
class TerrorNavArea;

class Director : public CAutoGameSystemPerFrame
{
	public:
		virtual void LevelInitPreEntity() override;
		//virtual void LevelInitPostEntity() override;
		//virtual void LevelShutdownPreEntity() override;
		//virtual void LevelShutdownPostEntity() override;
		virtual void FrameUpdatePostEntityThink() override;

	public:
		NavAreaVector m_northCandidateAreas;
		NavAreaVector m_southCandidateAreas;
		CountdownTimer m_CandidateUpdateTimer;
};

Director &TheDirector();

#endif