#include "cbase.h"
#include "l4dce_navmesh.h"
#include "l4dce_navarea.h"
#include "tier0/memdbgon.h"

extern ConVar nav_max_view_distance;
extern ConVar ZombiePopulation;

ConVar ZombiePopulation("z_population", "default");
ConVar DebugZombieBreakables("z_debug_breakables", "0");

TerrorNavMesh *TheTerrorNavMesh = static_cast<TerrorNavMesh *>(TheNavMesh);

unsigned int TerrorNavMesh::GetSubVersionNumber() const
{
	return 13;
}

CNavArea *TerrorNavMesh::CreateArea() const
{
	return new TerrorNavArea();
}

class TerrorPlaceDirectory
{
	public:
		typedef unsigned short IndexType;

		TerrorPlaceDirectory() { Reset(); }
		void Reset() { m_directory.RemoveAll(); }
		bool IsKnown(Place place) const { return m_directory.HasElement(place); }
		IndexType GetIndex(Place place) const;
		void AddPlace(Place place);
		Place IndexToPlace(IndexType entry) const;
		void Save(CUtlBuffer &fileBuffer);
		void Load(CUtlBuffer &fileBuffer);

		const CUtlVector<Place> *GetPlaces() const { return &m_directory; }

	private:
		CUtlVector<Place> m_directory;
};

TerrorPlaceDirectory::IndexType TerrorPlaceDirectory::GetIndex(Place place) const
{
	if(place == UNDEFINED_PLACE)
		return 0;

	int i = m_directory.Find(place);
	if(i == m_directory.InvalidIndex()) {
		AssertMsg(false, "TerrorPlaceDirectory::GetIndex failure");
		return 0;
	}

	return (IndexType)(i+1);
}

void TerrorPlaceDirectory::AddPlace(Place place)
{
	if(place == UNDEFINED_PLACE)
		return;

	Assert(place < 1000);

	if(IsKnown(place))
		return;

	m_directory.AddToTail(place);
}

Place TerrorPlaceDirectory::IndexToPlace(IndexType entry) const
{
	if(entry == 0)
		return UNDEFINED_PLACE;

	int i = entry-1;
	if(i >= m_directory.Count()) {
		AssertMsg(false, "TerrorPlaceDirectory::IndexToPlace: Invalid entry");
		return UNDEFINED_PLACE;
	}

	return m_directory.Element(i);
}

void TerrorPlaceDirectory::Save(CUtlBuffer &fileBuffer)
{
	IndexType count = (IndexType)m_directory.Count();
	fileBuffer.PutUnsignedShort(count);

	for(int i = 0; i < m_directory.Count(); i++) {
		const char *placeName = TheNavMesh->PlaceToName(m_directory.Element(i));
		unsigned short len = (unsigned short)(V_strlen(placeName) + 1);
		fileBuffer.PutUnsignedShort(len);
		fileBuffer.Put(placeName, len);
	}
}

void TerrorPlaceDirectory::Load(CUtlBuffer &fileBuffer)
{
	IndexType count = fileBuffer.GetUnsignedShort();

	m_directory.RemoveAll();

	char placeName[256];
	for(int i = 0; i < count; i++) {
		unsigned short len = fileBuffer.GetUnsignedShort();
		fileBuffer.Get(placeName, MIN(sizeof(placeName), len));

		Place place = TheNavMesh->NameToPlace(placeName);
		if(place == UNDEFINED_PLACE)
			Warning("Warning: NavMesh place %s is undefined?\n", placeName);
		AddPlace(place);
	}
}

TerrorPlaceDirectory fogPlaceDirectory;

void TerrorNavMesh::SaveCustomData(CUtlBuffer &fileBuffer) const
{
	BaseClass::SaveCustomData(fileBuffer);

	fileBuffer.PutFloat(nav_max_view_distance.GetFloat());
}

void TerrorNavMesh::LoadCustomData(CUtlBuffer &fileBuffer, unsigned int subVersion)
{
	BaseClass::LoadCustomData(fileBuffer, subVersion);

	if(subVersion >= 7)
		nav_max_view_distance.SetValue(fileBuffer.GetFloat());
}

void TerrorNavMesh::LoadFogPlaceDatabase()
{
	
}

void TerrorNavMesh::SaveCustomDataPreArea(CUtlBuffer &fileBuffer) const
{
	BaseClass::SaveCustomDataPreArea(fileBuffer);

	fileBuffer.PutString(ZombiePopulation.GetString());

	fogPlaceDirectory.Reset();
	FOR_EACH_VEC(TheNavAreas, i) {
		CNavArea *it = TheNavAreas.Element(i);
		Place place = it->GetPlace();
		fogPlaceDirectory.AddPlace(place);
	}
	fogPlaceDirectory.Save(fileBuffer);
}

void TerrorNavMesh::LoadCustomDataPreArea(CUtlBuffer &fileBuffer, unsigned int subVersion)
{
	BaseClass::LoadCustomDataPreArea(fileBuffer, subVersion);

	if(subVersion >= 8) {
		if(subVersion < 12) {
			ZombiePopulation.Revert();
		} else {
			char population[64];
			fileBuffer.GetString(population, sizeof(population));
			ZombiePopulation.SetValue(population);
		}
		fogPlaceDirectory.Load(fileBuffer);
	}
}

NavErrorType TerrorNavMesh::Load()
{
	nav_max_view_distance.SetValue(0.0f);
	LoadFogPlaceDatabase();
	fogPlaceDirectory.Reset();
	return BaseClass::Load();
}

void TerrorNavMesh::OnAreaCleared(TerrorNavArea *area)
{

}