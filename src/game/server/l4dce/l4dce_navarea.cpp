#include "cbase.h"
#include "l4dce_navarea.h"
#include "l4dce_navmesh.h"
#include "l4dce_player.h"
#include "tier0/memdbgon.h"

unsigned int TerrorNavArea::GetSpawnAttributes() const
{
	return m_SpawnAttributes;
}

void TerrorNavArea::RemoveSpawnAttributes(unsigned int attributes)
{
	m_SpawnAttributes &= ~attributes;
}

void TerrorNavArea::SetSpawnAttributes(unsigned int attributes)
{
	m_SpawnAttributes |= attributes;
}

void TerrorNavArea::Save(CUtlBuffer &fileBuffer, unsigned int version) const
{
	BaseClass::Save(fileBuffer, version);
	
	fileBuffer.PutUnsignedInt(m_SpawnAttributes);
	fileBuffer.PutUnsignedShort(0);
	fileBuffer.PutUnsignedInt(0);
	fileBuffer.PutUnsignedInt(0);
}

NavErrorType TerrorNavArea::Load(CUtlBuffer &fileBuffer, unsigned int version, unsigned int subVersion)
{
	NavErrorType type = BaseClass::Load(fileBuffer, version, subVersion);
	if(type != NAV_OK)
		return type;

	if(subVersion == 0)
		return NAV_OK;

	if(subVersion > TheNavMesh->GetSubVersionNumber()) {
		Warning("Unknown NavArea sub-version number (%i)\n", subVersion);
		return NAV_INVALID_FILE;
	}

	SetSpawnAttributes(fileBuffer.GetUnsignedInt());
	if(!fileBuffer.IsValid()) {
		Warning("Can't read spawn attributes\n");
		return NAV_INVALID_FILE;
	}

	if(subVersion >= 2) {
		if(subVersion <= 9)
			fileBuffer.GetUnsignedInt();
		if(subVersion >= 4) {
			if(subVersion <= 9)
				fileBuffer.GetUnsignedInt();
			if(subVersion <= 7)
				fileBuffer.GetUnsignedInt();
		}
	}

	if(subVersion >= 8)
		fileBuffer.GetUnsignedShort();

	if((GetSpawnAttributes() & 8) && (subVersion <= 9)) {
		fileBuffer.GetInt();
		fileBuffer.GetInt();
		fileBuffer.GetFloat();
		fileBuffer.GetFloat();
		fileBuffer.GetInt();
		RemoveSpawnAttributes(8);
	}

	if(subVersion <= 6) {
		unsigned char count = fileBuffer.GetUnsignedChar();
		for(unsigned char i = 0; i < count; i++)
			fileBuffer.GetUnsignedInt();
	}

	if(subVersion >= 6) {
		unsigned int visibleAreaCount = fileBuffer.GetUnsignedInt();
		m_potentiallyVisibleAreas.EnsureCapacity(visibleAreaCount);
		for(unsigned int i = 0; i < visibleAreaCount; i++) {
			AreaBindInfo info;
			info.id = fileBuffer.GetUnsignedInt();
			if(subVersion >= 9)
				info.attributes = fileBuffer.GetUnsignedChar();
			else
				info.attributes = POTENTIALLY_VISIBLE;
			m_potentiallyVisibleAreas.AddToTail(info);
		}
	}

	if(subVersion >= 11)
		m_inheritVisibilityFrom.id = fileBuffer.GetUnsignedInt();

	return NAV_OK;
}