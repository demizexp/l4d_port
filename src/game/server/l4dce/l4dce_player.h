#ifndef _L4DCE_PLAYER_H_
#define _L4DCE_PLAYER_H_

#pragma once

#include "basemultiplayerplayer.h"
#include "l4dce_shareddefs.h"
#include "l4dce_player_shared.h"

class TerrorNavArea;
enum PlayerAnimEvent_t;
class CTerrorPlayerAnimState;
class CTerrorViewModel;
class CTerrorWeaponBase;
extern int TrainSpeed(int iSpeed, int iMax);

class CTerrorPlayer : public CBaseMultiplayerPlayer
{
	public:
		DECLARE_CLASS(CTerrorPlayer, CBaseMultiplayerPlayer);
		DECLARE_SERVERCLASS();
		DECLARE_DATADESC();
		DECLARE_PREDICTABLE();

		CTerrorPlayer();
		virtual ~CTerrorPlayer();

		static CTerrorPlayer *CreatePlayer(const char *className, edict_t *ed);

		void SetSurvivor(TerrorSurvivors survivor);
		TerrorSurvivors GetSurvivor() const;
		virtual void SetAnimation(PLAYER_ANIM playerAnim);
		virtual void DoAnimationEvent(PlayerAnimEvent_t event, int nData=0);
		virtual void PostThink();
		virtual void Precache();
		virtual void Spawn();
		virtual CStudioHdr *OnNewModel();
		virtual int UpdateTransmitState();
		virtual void CheatImpulseCommands(int iImpulse);
		virtual CBaseEntity *EntSelectSpawnPoint( );
		virtual void Weapon_Equip( CBaseCombatWeapon *pWeapon );
		virtual bool BumpWeapon( CBaseCombatWeapon *pWeapon );
		virtual bool ClientCommand( const CCommand &args );
		virtual void PlayerUse ( void );
		void CreateViewModel( int iViewModel = 0 );
		const char *GetHandModelName() const;
		void HandleSpawnWeapons();
		virtual void SetFlashlightEnabled(bool bState) { m_bFlashLightEnabled = bState; }
		virtual int FlashlightIsOn();
		virtual bool FlashlightTurnOn(bool playSound = false);
		virtual void FlashlightTurnOff(bool playSound = false);

		CTerrorViewModel* GetViewModel( int vmIndex = VMINDEX_WEP ) const;
		void SetHandsModel( const char* model );
		const char* TranslatePlayerModelToHands();
		virtual void Event_Killed( const CTakeDamageInfo &info );
		CTerrorWeaponBase*	GetActiveTerrorWeapon() const;
		CUserCmd *GetCurrentCommand( void )	{ return m_pCurrentCommand; }
		virtual int GetSurvivorIndex( void ) { return m_nSurvivor.Get(); }

	protected:
		bool m_bFlashLightEnabled = true;
		CTerrorPlayerAnimState *m_pAnimState = nullptr;
		CNetworkQAngle(m_angEyeAngles);
		CNetworkVar(TerrorSurvivors, m_nSurvivor);
		CNetworkString(m_szArmName, MAX_WEAPON_STRING);
		bool m_bPlayUseDenySound = false;
};

CTerrorPlayer *ToTerrorPlayer(CBaseEntity *pEntity);

//----------------------------------------------------
// Definitions for weapon slots
//----------------------------------------------------
#define WEAPON_SLOT_PRIMARY		0
#define WEAPON_SLOT_SECONDARY	1
#define WEAPON_SLOT_THROWABLE	2
#define WEAPON_SLOT_DEPLOYABLE	3
#define WEAPON_SLOT_USEABLE		4

#endif