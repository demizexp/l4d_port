#ifndef L4DCE_SPAWNPOINT_H
#define L4DCE_SPAWNPOINT_H

class CSurvivorPosition : public CBaseEntity
{
public:
	DECLARE_CLASS(CSurvivorPosition, CBaseEntity)
	DECLARE_DATADESC();
	virtual bool KeyValue( const char *szKeyName, const char *szValue );
	virtual int GetSurvivorIndex() { return m_nSurvivor; }
	virtual void InputSetViewControl( inputdata_t &inputdata );
private:
	unsigned short int m_nSurvivor;
};

#endif