#ifndef _L4DCE_VIEWMODEL_H_
#define _L4DCE_VIEWMODEL_H_

#pragma once

#include "predicted_viewmodel.h"

#ifdef CLIENT_DLL
#define CTerrorViewModel C_TerrorViewModel
class C_TerrorViewModelAddon;
#endif

class CTerrorViewModel : public CPredictedViewModel
{
	private:
		DECLARE_CLASS(CTerrorViewModel, CPredictedViewModel);
		DECLARE_NETWORKCLASS();

	public:
		#ifdef CLIENT_DLL
		virtual ~CTerrorViewModel();
		virtual C_BaseAnimating* FindFollowedEntity();
		#endif

		virtual void SetWeaponModel(const char *pszModelname, CBaseCombatWeapon *weapon);

	private:
		#ifdef CLIENT_DLL
		C_TerrorViewModelAddon *m_pArms = nullptr;
		#endif
};

#ifdef CLIENT_DLL
class C_TerrorViewModelAddon : public C_PredictedViewModel
{
	private:
		DECLARE_CLASS(C_TerrorViewModelAddon, C_PredictedViewModel);

	public:
		virtual bool InitializeAsClientEntity(const char *pszModelName, bool bRenderWithViewModels);
		virtual int InternalDrawModel(int flags, const RenderableInstance_t &instance);
		virtual int	DrawModel(int flags, const RenderableInstance_t &instance);

		virtual CStudioHdr *OnNewModel();

		void UpdatePoseParams();
		void SetViewModel(C_TerrorViewModel *vm);

	private:
		int m_nMoveX = -1;
		int m_nVertAims = -1;
		CHandle<C_TerrorViewModel> m_hViewModel;
};
#endif

#endif