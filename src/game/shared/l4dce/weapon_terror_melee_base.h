#ifndef _TERROR_WEAPON_MELEE_BASE_H_
#define _TERROR_WEAPON_MELEE_BASE_H_

#pragma once

#include "weapon_terror_base.h"

#ifdef CLIENT_DLL
#define CTerrorWeaponMeleeBase C_TerrorWeaponMeleeBase
#endif

class CTerrorWeaponMeleeBase : public CTerrorWeaponBase
{
private:
	DECLARE_CLASS(CTerrorWeaponMeleeBase, CTerrorWeaponBase);
	DECLARE_NETWORKCLASS();
	DECLARE_PREDICTABLE();
	DECLARE_DATADESC();

public:
	CTerrorWeaponMeleeBase();
	
	//Attack functions
	virtual	void	PrimaryAttack( void );
	
	virtual void	ItemPostFrame( void );

	//Functions to select animation sequences 
	virtual Activity	GetPrimaryAttackActivity( void )	{	return	ACT_VM_HITCENTER;	}
	virtual Activity	GetSecondaryAttackActivity( void )	{	return	ACT_VM_HITCENTER2;	}

	virtual float		GetRange( void );
	virtual float		GetFireRate(void);

	void		AddViewKick( void );
	virtual float		GetDamageForActivity(Activity hitActivity);
	void		SecondaryAttack( void )	{	return;	}

	CTerrorWeaponMeleeBase(const CTerrorWeaponMeleeBase &);

protected:
	virtual	void	ImpactEffect( trace_t &trace );

private:
	bool			ImpactWater( const Vector &start, const Vector &end );
	void			Swing( int bIsSecondary );
	void			Hit( trace_t &traceHit, Activity nHitActivity );
	Activity		ChooseIntersectionPointAndActivity( trace_t &hitTrace, const Vector &mins, const Vector &maxs, CBasePlayer *pOwner );
};

#endif