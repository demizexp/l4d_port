#include "cbase.h"
#include "weapon_terror_shotgun_base.h"
#include "tier0/memdbgon.h"

#ifdef CLIENT_DLL
#define CWeaponShotgunChrome C_WeaponShotgunChrome
#endif

class CWeaponShotgunChrome : public CTerrorWeaponShotgunBase
{
	private:
		DECLARE_CLASS(CWeaponShotgunChrome, CTerrorWeaponShotgunBase);
		DECLARE_NETWORKCLASS();
		DECLARE_PREDICTABLE();
		DECLARE_DATADESC();

	public:
		CWeaponShotgunChrome();
};

CWeaponShotgunChrome::CWeaponShotgunChrome()
{
	m_bShotgunPumpAction = true;
	m_bShotgunAutomatic = false;
}

BEGIN_NETWORK_TABLE(CWeaponShotgunChrome, DT_WeaponShotgunChrome)
END_NETWORK_TABLE();

IMPLEMENT_NETWORKCLASS_ALIASED(WeaponShotgunChrome, DT_WeaponShotgunChrome);

BEGIN_PREDICTION_DATA(CWeaponShotgunChrome)
END_PREDICTION_DATA();

LINK_ENTITY_TO_CLASS(weapon_shotgun_chrome, CWeaponShotgunChrome);
PRECACHE_WEAPON_REGISTER(weapon_shotgun_chrome);

BEGIN_DATADESC(CWeaponShotgunChrome)
END_DATADESC();
