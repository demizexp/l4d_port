#include "cbase.h"
#include "weapon_terror_tactscope_base.h"
#include "tier0/memdbgon.h"

#ifdef CLIENT_DLL
#define CWeaponSMGUMP C_WeaponSMGUMP
#endif

class CWeaponSMGUMP : public CTerrorWeaponTactScopeBase
{
	private:
		DECLARE_CLASS(CWeaponSMGUMP, CTerrorWeaponTactScopeBase);
		DECLARE_NETWORKCLASS();
		DECLARE_PREDICTABLE();
		DECLARE_DATADESC();

public:
		
};

BEGIN_NETWORK_TABLE(CWeaponSMGUMP, DT_WeaponSMGUMP)
END_NETWORK_TABLE();

LINK_ENTITY_TO_CLASS(weapon_smg_ump, CWeaponSMGUMP);
PRECACHE_WEAPON_REGISTER(weapon_smg_ump);

IMPLEMENT_NETWORKCLASS_ALIASED(WeaponSMGUMP, DT_WeaponSMGUMP);

BEGIN_PREDICTION_DATA(CWeaponSMGUMP)
END_PREDICTION_DATA();

BEGIN_DATADESC(CWeaponSMGUMP)
END_DATADESC();

