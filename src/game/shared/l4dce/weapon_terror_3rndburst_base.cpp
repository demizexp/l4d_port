#include "cbase.h"
#include "weapon_terror_3rndburst_base.h"
#include "tier0/memdbgon.h"

LINK_ENTITY_TO_CLASS(terror_base_weapon_3rndburst, CTerrorWeapon3rndBurstBase);
PRECACHE_WEAPON_REGISTER(terror_base_weapon_3rndburst);

BEGIN_NETWORK_TABLE(CTerrorWeapon3rndBurstBase, DT_TerrorWeapon3rndBurstBase)
END_NETWORK_TABLE();

IMPLEMENT_NETWORKCLASS_ALIASED(TerrorWeapon3rndBurstBase, DT_TerrorWeapon3rndBurstBase);

BEGIN_PREDICTION_DATA(CTerrorWeapon3rndBurstBase)
END_PREDICTION_DATA();

BEGIN_DATADESC(CTerrorWeapon3rndBurstBase)
END_DATADESC();

//=========================================================
CTerrorWeapon3rndBurstBase::CTerrorWeapon3rndBurstBase()
{
}


bool CTerrorWeapon3rndBurstBase::Deploy(void)
{
	// Forget about any bursts this weapon was firing when holstered
	m_iBurstSize = 0;
	return BaseClass::Deploy();
}



//-----------------------------------------------------------------------------
// Purpose: Primary fire button attack
//-----------------------------------------------------------------------------
void CTerrorWeapon3rndBurstBase::BasePrimaryAttack(void)
{
	// If my clip is empty (and I use clips) start reload
	if (UsesClipsForAmmo1() && !m_iClip1)
	{
		Reload();
		return;
	}

	// Only the player fires this way so we can cast
	CBasePlayer *pPlayer = ToBasePlayer(GetOwner());

	//	if (!pPlayer)
	//	{
	//		return;
	//	}

	pPlayer->DoMuzzleFlash();

	SendWeaponAnim(GetPrimaryAttackActivity());

	// player "shoot" animation
	pPlayer->SetAnimation(PLAYER_ATTACK1);

	FireBulletsInfo_t info;
	info.m_vecSrc = pPlayer->Weapon_ShootPosition();

	info.m_vecDirShooting = pPlayer->GetAutoaimVector(AUTOAIM_SCALE_DEFAULT);

	// To make the firing framerate independent, we may have to fire more than one bullet here on low-framerate systems, 
	// especially if the weapon we're firing has a really fast rate of fire.
	info.m_iShots = 0;
	float fireRate = GetFireRate();

	while (m_flNextPrimaryAttack <= gpGlobals->curtime)
	{
		// MUST call sound before removing a round from the clip of a CMachineGun
		m_flNextPrimaryAttack = m_flNextPrimaryAttack + fireRate;
		info.m_iShots++;
		if (!fireRate)
			break;
	}

	info.m_flDistance = MAX_TRACE_LENGTH;
	info.m_iAmmoType = m_iPrimaryAmmoType;
	info.m_iTracerFreq = 2;

	float dmg = GetTerrorWpnData().flDamage;
	info.m_flDamage = dmg;
	info.m_flPlayerDamage = dmg;

#if !defined( CLIENT_DLL )
	// Fire the bullets
	info.m_vecSpread = pPlayer->GetAttackSpread(this);
#else
	//!!!HACKHACK - what does the client want this function for? 
	info.m_vecSpread = pPlayer->GetActiveWeapon()->GetBulletSpread();
#endif // CLIENT_DLL

	WeaponSound(SINGLE);
	info.m_iShots = 1;
	m_iClip1 -= info.m_iShots;

	pPlayer->FireBullets(info);

	if (!m_iClip1 && pPlayer->GetAmmoCount(m_iPrimaryAmmoType) <= 0)
	{
		// HEV suit - indicate out of ammo condition
		pPlayer->SetSuitUpdate("!HEV_AMO0", FALSE, 0);
	}

	//Add our view kick in
	AddViewKick();
}

//-----------------------------------------------------------------------------
// Purpose: 
//
//
//-----------------------------------------------------------------------------
void CTerrorWeapon3rndBurstBase::BurstThink(void)
{
	BasePrimaryAttack();

	m_iBurstSize--;

	if (m_iBurstSize == 0)
	{
		// The burst is over!
		SetThink(NULL);

		// idle immediately to stop the firing animation
		SetWeaponIdleTime(gpGlobals->curtime);
		return;
	}

	SetNextThink(gpGlobals->curtime + 0.05f);
}

//-----------------------------------------------------------------------------
// Purpose: 
//
//
//-----------------------------------------------------------------------------
void CTerrorWeapon3rndBurstBase::PrimaryAttack(void)
{
	if (m_bFireOnEmpty)
	{
		return;
	}
	
	{
		m_iBurstSize = GetBurstSize();

		// Call the think function directly so that the first round gets fired immediately.
		BurstThink();
		SetThink(&CTerrorWeapon3rndBurstBase::BurstThink);
		m_flNextPrimaryAttack = gpGlobals->curtime + GetBurstCycleRate();
		m_flNextSecondaryAttack = gpGlobals->curtime + GetBurstCycleRate();

		// Pick up the rest of the burst through the think function.
		SetNextThink(gpGlobals->curtime + GetFireRate());
	}
}

float CTerrorWeapon3rndBurstBase::GetBurstCycleRate( void )
{
	// this is the time it takes to fire an entire 
	// burst, plus whatever amount of delay we want
	// to have between bursts.
	//return GetTerrorWpnData().flCycleTime;
	return 0.5f;
}

