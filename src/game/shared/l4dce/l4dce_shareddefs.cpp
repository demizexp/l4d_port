#include "cbase.h"
#include "l4dce_shareddefs.h"
#include "tier0/memdbgon.h"

bool IsL4D1Survivor(TerrorSurvivors survivor)
{
	return ((survivor >= FIRST_L4D1SURVIVOR) && (survivor <= LAST_L4D1SURVIVOR));
}

bool IsL4D2Survivor(TerrorSurvivors survivor)
{
	return ((survivor >= FIRST_L4D2SURVIVOR) && (survivor <= LAST_L4D2SURVIVOR));
}


bool IsL4DCESurvivor(TerrorSurvivors survivor)
{
	return ((survivor >= FIRST_L4DCESURVIVOR) && (survivor <= LAST_L4DCESURVIVOR));
}

const char *const GetNameForSurvivor(TerrorSurvivors survivor)
{
	static const char *const default = "Invalid!!!";

	//MUST MATCH TerrorSurvivors!!!
	static const char *const array[NUM_SURVIVORS] = 
	{
		default, //SURVIVOR_INVALID

		"Coach", //SURVIVOR_COACH
		"Gambler", //SURVIVOR_NICK
		"Mechanic", //SURVIVOR_ELLIS
		"Producer", //SURVIVOR_ROCHELLE

		"TeenAngst", //SURVIVOR_ZOEY
		"NamVet", //SURVIVOR_BILL
		"Biker", //SURVIVOR_FRANCIS
		"Manager", //SURVIVOR_LOUIS

		"Crazy", //SURVIVOR_KEITH
		"Pilot", //SURVIVOR_MICHAEL
		"GasMask", //SURVIVOR_BRAIR
		"Fallen", //SURVIVOR_WHITAKER
	};
	static const int size = ARRAYSIZE(array);

	if(survivor >= size)
		return default;

	return array[survivor];
}

const char *const GetModelForSurvivor(TerrorSurvivors survivor)
{
	static const int size = ARRAYSIZE(g_ppszPlayerModels);

	if(survivor >= size)
		return "models/error.mdl";

	return g_ppszPlayerModels[survivor];
}

const char *const GetArmForSurvivor(TerrorSurvivors survivor)
{
	static const int size = ARRAYSIZE(g_ppszArmModels);

	if(survivor >= size)
		return "models/error.mdl";

	return g_ppszArmModels[survivor];
}

#ifdef DEBUG
void Debug_UnlockCvars()
{
	#ifdef CLIENT_DLL
	engine->SetRestrictClientCommands(false);
	engine->SetRestrictServerCommands(false);
	#endif

	static const int badflags = FCVAR_UNREGISTERED|FCVAR_DEVELOPMENTONLY|
		FCVAR_HIDDEN|FCVAR_PROTECTED|FCVAR_SPONLY|
		FCVAR_CHEAT|FCVAR_PRINTABLEONLY|
		FCVAR_NEVER_AS_STRING|FCVAR_NOT_CONNECTED|
		FCVAR_SERVER_CANNOT_QUERY|FCVAR_NOTIFY;

	static const int goodflags = FCVAR_SERVER_CAN_EXECUTE|
		FCVAR_CLIENTCMD_CAN_EXECUTE|FCVAR_RELEASE|FCVAR_ACCESSIBLE_FROM_THREADS;

	ICvar::Iterator iter(cvar);
	for(iter.SetFirst(); iter.IsValid(); iter.Next()) {
		ConCommandBase *pConBase = iter.Get();
		pConBase->RemoveFlags(badflags);
		pConBase->AddFlags(goodflags);
	}
}
#endif