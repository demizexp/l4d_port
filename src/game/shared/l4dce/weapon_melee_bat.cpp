#include "cbase.h"
#include "weapon_terror_melee_base.h"
#include "tier0/memdbgon.h"

#ifdef CLIENT_DLL
#define CWeaponMeleeBat C_WeaponMeleeBat
#endif

class CWeaponMeleeBat : public CTerrorWeaponMeleeBase
{
private:
	DECLARE_CLASS(CWeaponMeleeBat, CTerrorWeaponMeleeBase);
	DECLARE_NETWORKCLASS();
	DECLARE_PREDICTABLE();
	DECLARE_DATADESC();

public:

};

BEGIN_NETWORK_TABLE(CWeaponMeleeBat, DT_WeaponMeleeBat)
END_NETWORK_TABLE();

LINK_ENTITY_TO_CLASS(weapon_melee_bat, CWeaponMeleeBat);
PRECACHE_WEAPON_REGISTER(weapon_melee_bat);

IMPLEMENT_NETWORKCLASS_ALIASED(WeaponMeleeBat, DT_WeaponMeleeBat);

BEGIN_PREDICTION_DATA(CWeaponMeleeBat)
END_PREDICTION_DATA();

BEGIN_DATADESC(CWeaponMeleeBat)
END_DATADESC();

