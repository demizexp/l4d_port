#include "cbase.h"
#include "l4dce_missioninfo.h"
#include "filesystem.h"
#include "tier0/memdbgon.h"

const CMissionInfo &MissionInfo()
{
	static CMissionInfo missioninfo;
	return missioninfo;
}

CMissionInfo::CMissionInfo()
{
	FileFindHandle_t handle;
	const char *filename = filesystem->FindFirstEx("missions/campaign*.txt", "GAME", &handle);
	while(filename) {

		char file[FILENAME_MAX];
		Q_snprintf(file, sizeof(file), "missions/%s", filename);
		KeyValues *keyvalues = new KeyValues("");

		if(!keyvalues->LoadFromFile(filesystem, file, "GAME")) 
		{
			filename = filesystem->FindNext(handle);
			keyvalues->deleteThis();
			continue;
		}

		m_Campaigns.AddToTail(keyvalues);

		filename = filesystem->FindNext(handle);
	}
	filesystem->FindClose(handle);
}

KeyValues *CMissionInfo::GetCampaignDetails(const char *name) const
{
	for (int i = 0; i < m_Campaigns.Count(); i++)
	{
		const char* CampaignName = m_Campaigns[i]->GetString("Name");
		if  (stricmp( CampaignName, name) == 0 )
		{
			return m_Campaigns[i];
		}
	}
	return NULL;
}
int CMissionInfo::GetCount( void ) const
{
	return m_Campaigns.Count();
}

CMissionInfo::~CMissionInfo()
{
	for(int i = 0; i < m_Campaigns.Count(); i++) {
		KeyValues *&it = m_Campaigns.Element(i);
		it->deleteThis();
		it = nullptr;
	}
}