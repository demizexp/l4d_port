#include "cbase.h"
#include "weapon_terror_3rndburst_base.h"
#include "tier0/memdbgon.h"

#ifdef CLIENT_DLL
#define CWeaponSMGMP5 C_WeaponSMGMP5
#endif

class CWeaponSMGMP5 : public CTerrorWeapon3rndBurstBase
{
	private:
		DECLARE_CLASS(CWeaponSMGMP5, CTerrorWeapon3rndBurstBase);
		DECLARE_NETWORKCLASS();
		DECLARE_PREDICTABLE();
		DECLARE_DATADESC();

	public:
		
};

LINK_ENTITY_TO_CLASS(weapon_smg_mp5, CWeaponSMGMP5);
PRECACHE_WEAPON_REGISTER(weapon_smg_mp5);

BEGIN_NETWORK_TABLE(CWeaponSMGMP5, DT_WeaponSMGMP5)
END_NETWORK_TABLE();

IMPLEMENT_NETWORKCLASS_ALIASED(WeaponSMGMP5, DT_WeaponSMGMP5);

BEGIN_PREDICTION_DATA(CWeaponSMGMP5)
END_PREDICTION_DATA();

BEGIN_DATADESC(CWeaponSMGMP5)
END_DATADESC();
