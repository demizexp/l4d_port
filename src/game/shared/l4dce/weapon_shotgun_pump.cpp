#include "cbase.h"
#include "weapon_terror_shotgun_base.h"
#include "tier0/memdbgon.h"

#ifdef CLIENT_DLL
#define CWeaponShotgunPump C_WeaponShotgunPump
#endif

class CWeaponShotgunPump : public CTerrorWeaponShotgunBase
{
	private:
		DECLARE_CLASS(CWeaponShotgunPump, CTerrorWeaponShotgunBase);
		DECLARE_NETWORKCLASS();
		DECLARE_PREDICTABLE();
		DECLARE_DATADESC();
		
	public:
		CWeaponShotgunPump();
		void Spawn();
};

CWeaponShotgunPump::CWeaponShotgunPump()
{
	m_bShotgunPumpAction = true;
	m_bShotgunAutomatic = false;
}

void CWeaponShotgunPump::Spawn()
{
	SetClassname("weapon_shotgun_pump"); // hack to allow for old names
	BaseClass::Spawn();
}

BEGIN_NETWORK_TABLE(CWeaponShotgunPump, DT_WeaponShotgunPump)
END_NETWORK_TABLE();

IMPLEMENT_NETWORKCLASS_ALIASED(WeaponShotgunPump, DT_WeaponShotgunPump);

BEGIN_PREDICTION_DATA(CWeaponShotgunPump)
END_PREDICTION_DATA();

#ifndef CLIENT_DLL
LINK_ENTITY_TO_CLASS(weapon_pumpshotgun, CWeaponShotgunPump);
#endif

LINK_ENTITY_TO_CLASS(weapon_shotgun_pump, CWeaponShotgunPump);
PRECACHE_WEAPON_REGISTER(weapon_shotgun_pump);

BEGIN_DATADESC(CWeaponShotgunPump)
END_DATADESC();
