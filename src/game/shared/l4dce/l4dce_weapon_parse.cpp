#include "cbase.h"
#include "l4dce_weapon_parse.h"
#include "tier0/memdbgon.h"

#pragma warning (disable : 4800)

FileWeaponInfo_t *CreateWeaponInfo()
{
	return new CTerrorWeaponInfo();
}

CTerrorWeaponInfo::CTerrorWeaponInfo()
	: BaseClass()
{
	
}

void CTerrorWeaponInfo::Parse(KeyValues *pKeyValuesData, const char *szWeaponName)
{
	BaseClass::Parse(pKeyValuesData, szWeaponName);

	iBullets = pKeyValuesData->GetInt("Bullets", 1);
	flDamage = pKeyValuesData->GetFloat("Damage", 0.0f);
	bNewInL4D2 = pKeyValuesData->GetBool("NewInL4D2", false);
	iTier = pKeyValuesData->GetInt("Tier", 0);
	flCycleTime = pKeyValuesData->GetFloat("CycleTime", 0.0f);

	flSpreadPerShot = sin(pKeyValuesData->GetFloat("SpreadPerShot", 0.0f) / 20.0f);
	vecSpreadPerShot = Vector(flSpreadPerShot, flSpreadPerShot, flSpreadPerShot);

	Q_strncpy(szAddonAttachment, pKeyValuesData->GetString("AddonAttachment", "primary"), MAX_WEAPON_STRING);
	Q_strncpy(szDisplayName, pKeyValuesData->GetString("DisplayName", WEAPON_PRINTNAME_MISSING), MAX_WEAPON_STRING);
	Q_strncpy(szDisplayNameCaps, pKeyValuesData->GetString("DisplayNameAllCaps", WEAPON_PRINTNAME_MISSING), MAX_WEAPON_STRING);

	Q_strncpy(szViewModelDual, pKeyValuesData->GetString("viewmodel_dual"), MAX_WEAPON_STRING);
	Q_strncpy(szWorldModelDual, pKeyValuesData->GetString("playermodel_dual"), MAX_WEAPON_STRING);
	Q_strncpy(szWorldPickupModel, pKeyValuesData->GetString("worldmodel"), MAX_WEAPON_STRING);

	iDamageFlags = pKeyValuesData->GetInt("damage_flags", 0);
	flWeaponIdleTime = pKeyValuesData->GetFloat("weapon_idle_time", 0.0f);
	flReFireDelay = pKeyValuesData->GetFloat("refire_delay", 0.2f);
	iRange = pKeyValuesData->GetInt("Range", 0);

	m_bAccuracyQuadratic= pKeyValuesData->GetInt( "AccuracyQuadratic", 0 );
	m_flAccuracyDivisor	= pKeyValuesData->GetFloat( "AccuracyDivisor", -1 ); // -1 = off
	m_flAccuracyOffset	= pKeyValuesData->GetFloat( "AccuracyOffset", 0 );
	m_flMaxInaccuracy	= pKeyValuesData->GetFloat( "MaxInaccuracy", 0 );

	bUseL4DDrawAct = pKeyValuesData->GetBool("DeployNotDraw", true);
}

