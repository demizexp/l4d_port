#include "cbase.h"
#include "weapon_terror_scope_base.h"
#include "tier0/memdbgon.h"

#ifdef CLIENT_DLL
#define CWeaponSniperScout C_WeaponSniperScout
#endif

class CWeaponSniperScout : public CTerrorWeaponScopeBase
{
	private:
		DECLARE_CLASS(CWeaponSniperScout, CTerrorWeaponScopeBase);
		DECLARE_NETWORKCLASS();
		DECLARE_PREDICTABLE();
		DECLARE_DATADESC();

	public:
		
};

BEGIN_NETWORK_TABLE(CWeaponSniperScout, DT_WeaponSniperScout)
END_NETWORK_TABLE();

LINK_ENTITY_TO_CLASS(weapon_sniper_scout, CWeaponSniperScout);
PRECACHE_WEAPON_REGISTER(weapon_sniper_scout);

IMPLEMENT_NETWORKCLASS_ALIASED(WeaponSniperScout, DT_WeaponSniperScout);

BEGIN_PREDICTION_DATA(CWeaponSniperScout)
END_PREDICTION_DATA();

BEGIN_DATADESC(CWeaponSniperScout)
END_DATADESC();

