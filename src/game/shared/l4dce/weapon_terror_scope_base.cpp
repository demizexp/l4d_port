#include "cbase.h"
#include "weapon_terror_scope_base.h"
#include "tier0/memdbgon.h"
#include "in_buttons.h"


LINK_ENTITY_TO_CLASS(terror_base_weapon_scope, CTerrorWeaponScopeBase);
PRECACHE_WEAPON_REGISTER(terror_base_weapon_scope);

BEGIN_NETWORK_TABLE(CTerrorWeaponScopeBase, DT_TerrorWeaponScopeBase)
END_NETWORK_TABLE();

IMPLEMENT_NETWORKCLASS_ALIASED(TerrorWeaponScopeBase, DT_TerrorWeaponScopeBase);

BEGIN_PREDICTION_DATA(CTerrorWeaponScopeBase)
END_PREDICTION_DATA();

BEGIN_DATADESC(CTerrorWeaponScopeBase)
END_DATADESC();

acttable_t CTerrorWeaponScopeBase::m_acttable[] =
{
	{ ACT_MP_STAND_IDLE,	ACT_IDLE_PISTOL, false },
	{ ACT_MP_CROUCH_IDLE,	ACT_CROUCHIDLE_PISTOL, false },
	{ ACT_MP_RUN,			ACT_RUN_PISTOL, false },
	{ ACT_MP_CROUCHWALK,	ACT_RUN_CROUCH_PISTOL, false },
	{ ACT_MP_JUMP,			ACT_JUMP_PISTOL, false },
	{ ACT_MP_RELOAD_STAND,	ACT_RELOAD_PISTOL, false },
	{ ACT_MP_RELOAD_CROUCH, ACT_RELOAD_PISTOL, false },
	{ ACT_RANGE_ATTACK1,	ACT_PRIMARYATTACK_PISTOL, false },
};
IMPLEMENT_ACTTABLE(CTerrorWeaponScopeBase);

void CTerrorWeaponScopeBase::PrimaryAttack()
{
	CTerrorPlayer *pPlayer = GetPlayerOwner();
	if (!(pPlayer->m_afButtonPressed & IN_ATTACK))
		return;

	BaseClass::PrimaryAttack();
}
