#include "cbase.h"
#include "weapon_terror_scope_base.h"
#include "tier0/memdbgon.h"

#ifdef CLIENT_DLL
#define CWeaponSniperAWP C_WeaponSniperAWP
#endif

class CWeaponSniperAWP : public CTerrorWeaponScopeBase
{
	private:
		DECLARE_CLASS(CWeaponSniperAWP, CTerrorWeaponScopeBase);
		DECLARE_NETWORKCLASS();
		DECLARE_PREDICTABLE();
		DECLARE_DATADESC();

	public:
		
};

BEGIN_NETWORK_TABLE(CWeaponSniperAWP, DT_WeaponSniperAWP)
END_NETWORK_TABLE();

LINK_ENTITY_TO_CLASS(weapon_sniper_awp, CWeaponSniperAWP);
PRECACHE_WEAPON_REGISTER(weapon_sniper_awp);

IMPLEMENT_NETWORKCLASS_ALIASED(WeaponSniperAWP, DT_WeaponSniperAWP);

BEGIN_PREDICTION_DATA(CWeaponSniperAWP)
END_PREDICTION_DATA();

BEGIN_DATADESC(CWeaponSniperAWP)
END_DATADESC();

