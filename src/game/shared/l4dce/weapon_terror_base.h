#ifndef _TERROR_WEAPON_BASE_H_
#define _TERROR_WEAPON_BASE_H_

#pragma once

#include "basecombatweapon_shared.h"
#include "l4dce_player_shared.h"
#include "l4dce_weapon_parse.h"
#include "l4dce_viewmodel.h" 
#include "in_buttons.h"

#ifdef CLIENT_DLL
#define CTerrorWeaponBase C_TerrorWeaponBase
#define CTerrorPlayer C_TerrorPlayer
#endif

class CTerrorWeaponBase : public CBaseCombatWeapon
{
	private:
		DECLARE_CLASS(CTerrorWeaponBase, CBaseCombatWeapon);
		DECLARE_NETWORKCLASS();
		DECLARE_DATADESC();
		DECLARE_PREDICTABLE();
		DECLARE_ACTTABLE();

	public:
		CTerrorWeaponBase();
		
#ifdef CLIENT_DLL
		virtual bool ShouldPredict();
#endif

		const CTerrorWeaponInfo &GetTerrorWpnData() const;
		CTerrorPlayer *GetPlayerOwner() const;

		virtual bool IsPredicted() const;
		virtual void PrimaryAttack();
		virtual void SecondaryAttack();
		virtual void MeleeAttack();
		virtual bool Reload();
		virtual void ItemPostFrame();
		virtual float GetFireRate();
		virtual const Vector& GetBulletSpread();
		virtual Activity GetDrawActivity();
		virtual Activity GetMeleeAttackActivity();
		virtual void AddViewmodelBob(CBaseViewModel *viewmodel, Vector &origin, QAngle &angles);
		virtual	float CalcViewmodelBob(void);
		virtual const char *GetViewModel(int iViewModel) const;
		virtual void  Equip( CBaseCombatCharacter *pOwner ); 
		virtual void SetPickupTouch( void );

		virtual void SetWeaponVisible( bool visible );
		virtual bool Holster(CBaseCombatWeapon* pSwitchTo);
		virtual bool Deploy();

		bool ShoveImpactWater( const Vector &start, const Vector &end );
		void ShoveSwing( int bIsSecondary );
		void ShoveHit( trace_t &traceHit, Activity nHitActivity );
		Activity ShoveChooseIntersectionPointAndActivity( trace_t &hitTrace, const Vector &mins, const Vector &maxs, CBasePlayer *pOwner );
		virtual	void ShoveImpactEffect( trace_t &trace );
		virtual float ShoveGetRange( void );

		bool m_bPickedUpAlready;
		CNetworkVar(float, m_flNextMeleeAttack);	// soonest time ItemPostFrame will call MeleeAttack
		CNetworkVar(float, m_flReloadTime);			// full time for reload animation (not on shotguns)
		CNetworkVar(bool, m_bReloadIsSuspended);	// reload is suspended for melee attack
};

#endif