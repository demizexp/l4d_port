#ifndef _L4DCE_MISSION_INFO_H_
#define _L4DCE_MISSION_INFO_H_

#pragma once

#include "igamesystem.h"

class CMissionInfo
{
	public:
		CMissionInfo();
		~CMissionInfo();

		KeyValues *GetCampaignDetails(const char *name) const;
		int	GetCount( void ) const;

	private:
		CUtlVector<KeyValues *> m_Campaigns;
};

const CMissionInfo &MissionInfo();

#endif