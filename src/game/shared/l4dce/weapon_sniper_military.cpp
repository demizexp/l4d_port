#include "cbase.h"
#include "weapon_terror_scope_base.h"
#include "tier0/memdbgon.h"

#ifdef CLIENT_DLL
#define CWeaponSniperMilitary C_WeaponSniperMilitary
#endif

class CWeaponSniperMilitary : public CTerrorWeaponScopeBase
{
	private:
		DECLARE_CLASS(CWeaponSniperMilitary, CTerrorWeaponScopeBase);
		DECLARE_NETWORKCLASS();
		DECLARE_PREDICTABLE();
		DECLARE_DATADESC();

	public:
		
};

BEGIN_NETWORK_TABLE(CWeaponSniperMilitary, DT_WeaponSniperMilitary)
END_NETWORK_TABLE();

LINK_ENTITY_TO_CLASS(weapon_sniper_military, CWeaponSniperMilitary);
PRECACHE_WEAPON_REGISTER(weapon_sniper_military);

IMPLEMENT_NETWORKCLASS_ALIASED(WeaponSniperMilitary, DT_WeaponSniperMilitary);

BEGIN_PREDICTION_DATA(CWeaponSniperMilitary)
END_PREDICTION_DATA();

BEGIN_DATADESC(CWeaponSniperMilitary)
END_DATADESC();

