#include "cbase.h"
#include "weapon_terror_shotgun_base.h"
#include "tier0/memdbgon.h"

#ifdef CLIENT_DLL
#define CWeaponShotgunAuto C_WeaponShotgunAuto
#endif

class CWeaponShotgunAuto : public CTerrorWeaponShotgunBase
{
	private:
		DECLARE_CLASS(CWeaponShotgunAuto, CTerrorWeaponShotgunBase);
		DECLARE_NETWORKCLASS();
		DECLARE_PREDICTABLE();
		DECLARE_DATADESC();

	public:
		CWeaponShotgunAuto();
		void Spawn();
};

CWeaponShotgunAuto::CWeaponShotgunAuto()
{
	m_bShotgunPumpAction = false;
	m_bShotgunAutomatic = false;
}

void CWeaponShotgunAuto::Spawn()
{
	SetClassname("weapon_shotgun_auto"); // hack to allow for old names
	BaseClass::Spawn();
}

BEGIN_NETWORK_TABLE(CWeaponShotgunAuto, DT_WeaponShotgunAuto)
END_NETWORK_TABLE();

#ifndef CLIENT_DLL
LINK_ENTITY_TO_CLASS(weapon_autoshotgun, CWeaponShotgunAuto);
#endif

LINK_ENTITY_TO_CLASS(weapon_shotgun_auto, CWeaponShotgunAuto);
PRECACHE_WEAPON_REGISTER(weapon_shotgun_auto);

IMPLEMENT_NETWORKCLASS_ALIASED(WeaponShotgunAuto, DT_WeaponShotgunAuto);

BEGIN_PREDICTION_DATA(CWeaponShotgunAuto)
END_PREDICTION_DATA();

BEGIN_DATADESC(CWeaponShotgunAuto)
END_DATADESC();
