#ifndef _TERROR_WEAPON_3RNDBURST_BASE_H_
#define _TERROR_WEAPON_3RNDBURST_BASE_H_

#pragma once

#include "weapon_terror_base.h"

#ifdef CLIENT_DLL
#define CTerrorWeapon3rndBurstBase C_TerrorWeapon3rndBurstBase
#endif

class CTerrorWeapon3rndBurstBase : public CTerrorWeaponBase
{
	private:
		DECLARE_CLASS(CTerrorWeapon3rndBurstBase, CTerrorWeaponBase);
		DECLARE_NETWORKCLASS();
		DECLARE_PREDICTABLE();
		DECLARE_DATADESC();

	public:
		CTerrorWeapon3rndBurstBase();
		void			PrimaryAttack( void );
		void			BasePrimaryAttack( void );
		void			BurstThink( void );
		bool			Deploy();
		virtual float	GetBurstCycleRate( void );
		virtual int		GetBurstSize( void ) { return 3; };

	protected:
		int m_iBurstSize;
};

#endif