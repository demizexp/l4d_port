#ifndef _L4DCE_SHAREDEFS_H_
#define _L4DCE_SHAREDEFS_H_

#pragma once

//order matters do not change
enum TerrorSurvivors
{
	SURVIVOR_INVALID,

	SURVIVOR_COACH,		//Coach
	SURVIVOR_NICK,		//Gambler
	SURVIVOR_ELLIS,		//Mechanic
	SURVIVOR_ROCHELLE,	//Producer

	SURVIVOR_ZOEY,		//TeenAngst
	SURVIVOR_BILL,		//NamVet
	SURVIVOR_FRANCIS,	//Biker
	SURVIVOR_LOUIS,		//Manager

	SURVIVOR_KEITH,		//Crazy
	SURVIVOR_MICHAEL,	//Pilot
	SURVIVOR_BRAIR,		//GasMask
	SURVIVOR_WHITAKER,	//Fallen

	NUM_SURVIVORS,
};

enum TerrorInfectedTypes
{
	INFECTED_INVAILD,

	INFECTED_COMMON,	//basic infected
	INFECTED_UNCOMMON,	//less common basic infected with special abilities

	INFECTED_BOOMER,	//Vomits on players to attract the horde, explodes on death
	INFECTED_HUNTER,	//tackles down the player and claws them to pieces
	INFECTED_SMOKER,	//grapples player with tongue, explodes into a smokegrenade on death

	INFECTED_CHARGER,	//runs at like a football player, holds and beats the player
	INFECTED_JOCKEY,	//jumps onto player's back, pulling in random directions and punching
	INFECTED_SPITTER,	//spits acid at the player, leaves a (smaller) puddle on death

	INFECTED_WITCH,		//Boss zombie, either sits down or wonders randomly, instantly downs whoever startles it
	INFECTED_TANK,		//Boss zombie, giant, runs at the nearest player, throws rocks

	INFECTED_SCREAMER,	//Restored cut boss zombie, runs from the survivors while attracting the horde
	INFECTED_POSIONER,	//New spitter-like zombie, applies a (less harmful) version of HL2's posion zombie effects

	NUM_INFECTED,
};

enum TerrorInfectedStyles
{
	INFECTEDSTYLE_INVAILD,

	INFECTEDSTYLE_DEFAULT,	//L4D2 male
	INFECTEDSTYLE_FEMALE,	//L4D2 female
	INFECTEDSTYLE_L4D1,		//L4D1

	NUM_INFECTEDSTYLES,
};

//MUST MATCH TerrorSurvivors!!!
static const char* g_ppszArmModels[NUM_SURVIVORS] =
{
	"models/error.mdl", //SURVIVOR_INVALID

	"models/weapons/arms/v_arms_coach.mdl", //SURVIVOR_COACH
	"models/weapons/arms/v_arms_gambler.mdl", //SURVIVOR_NICK
	"models/weapons/arms/v_arms_mechanic.mdl", //SURVIVOR_ELLIS
	"models/weapons/arms/v_arms_producer.mdl", //SURVIVOR_ROCHELLE

	"models/weapons/arms/v_arms_teenangst.mdl", //SURVIVOR_ZOEY
	"models/weapons/arms/v_arms_namvet.mdl", //SURVIVOR_BILL
	"models/weapons/arms/v_arms_biker.mdl", //SURVIVOR_FRANCIS
	"models/weapons/arms/v_arms_manager.mdl", //SURVIVOR_LOUIS

	"models/weapons/arms/v_arms_crazy.mdl", //SURVIVOR_KEITH
	"models/weapons/arms/v_arms_pilot.mdl", //SURVIVOR_MICHAEL
	"models/weapons/arms/v_arms_gasmask.mdl", //SURVIVOR_BRAIR
	"models/weapons/arms/v_arms_fallen.mdl", //SURVIVOR_WHITAKER
};


//MUST MATCH TerrorSurvivors!!!
static const char *const g_ppszPlayerModels[NUM_SURVIVORS] =
{
	"models/error.mdl", //SURVIVOR_INVALID

	"models/survivors/survivor_coach.mdl", //SURVIVOR_COACH
	"models/survivors/survivor_gambler.mdl", //SURVIVOR_NICK
	"models/survivors/survivor_mechanic.mdl", //SURVIVOR_ELLIS
	"models/survivors/survivor_producer.mdl", //SURVIVOR_ROCHELLE

	"models/survivors/survivor_teenangst.mdl", //SURVIVOR_ZOEY
	"models/survivors/survivor_namvet.mdl", //SURVIVOR_BILL
	"models/survivors/survivor_biker.mdl", //SURVIVOR_FRANCIS
	"models/survivors/survivor_manager.mdl", //SURVIVOR_LOUIS

	"models/survivors/survivor_crazy.mdl", //SURVIVOR_KEITH
	"models/survivors/survivor_pilot.mdl", //SURVIVOR_MICHAEL
	"models/survivors/survivor_gasmask.mdl", //SURVIVOR_BRAIR
	"models/survivors/survivor_fallen.mdl", //SURVIVOR_WHITAKER
};

bool IsL4D1Survivor(TerrorSurvivors survivor);
bool IsL4D2Survivor(TerrorSurvivors survivor);
bool IsL4DCESurvivor(TerrorSurvivors survivor);

const char *const GetNameForSurvivor(TerrorSurvivors survivor);
const char *const GetModelForSurvivor(TerrorSurvivors survivor);
const char *const GetArmForSurvivor(TerrorSurvivors survivor);

#define FIRST_SURVIVOR (SURVIVOR_INVALID+1)
#define LAST_SURVIVOR (NUM_SURVIVORS-1)

#define FIRST_L4D1SURVIVOR (SURVIVOR_ZOEY)
#define LAST_L4D1SURVIVOR (SURVIVOR_LOUIS)

#define FIRST_L4D2SURVIVOR (SURVIVOR_COACH)
#define LAST_L4D2SURVIVOR (SURVIVOR_ROCHELLE)

#define FIRST_L4DCESURVIVOR (SURVIVOR_KEITH)
#define LAST_L4DCESURVIVOR (SURVIVOR_WHITAKER)

#define TEAM_SURVIVOR (FIRST_GAME_TEAM)
#define TEAM_INFECTED (FIRST_GAME_TEAM+1)

#define MASK_ZOMBIESOLID_BRUSHONLY (MASK_NPCSOLID_BRUSHONLY)

#define VMINDEX_WEP         0
#define VMINDEX_HANDS       1

#endif