#include "cbase.h"
#include "l4dce_viewmodel.h"
#ifdef CLIENT_DLL
#include "l4dce_player.h"
#endif
#include "tier0/memdbgon.h"

LINK_ENTITY_TO_CLASS(terror_viewmodel, CTerrorViewModel);

IMPLEMENT_NETWORKCLASS_ALIASED(TerrorViewModel, DT_TerrorViewModel);

BEGIN_NETWORK_TABLE(CTerrorViewModel, DT_TerrorViewModel)
END_NETWORK_TABLE();

#ifdef CLIENT_DLL
CTerrorViewModel::~CTerrorViewModel()
{
	if(m_pArms)
		m_pArms->Remove();
	m_pArms = nullptr;
}
#endif

void CTerrorViewModel::SetWeaponModel(const char *pszModelname, CBaseCombatWeapon *weapon)
{
	BaseClass::SetWeaponModel(pszModelname, weapon);

	/*#ifdef CLIENT_DLL
	C_TerrorPlayer *pOwner = static_cast<C_TerrorPlayer *>(GetOwner());
	if(!pOwner->IsLocalPlayer())
		return;

	const char *hand = pOwner->GetHandModelName();
	
	if(!m_pArms) {
		m_pArms = new C_TerrorViewModelAddon();
		m_pArms->InitializeAsClientEntity(hand, true);
	}

	m_pArms->SetViewModel(this);
	m_pArms->SetWeaponModel(hand, weapon);
	m_pArms->FollowEntity(this, true);
	m_pArms->SetAbsOrigin(GetAbsOrigin());
	m_pArms->InvalidateBoneCache();
	m_pArms->InvalidateBoneCaches();
	#endif*/
}

#ifdef CLIENT_DLL
C_BaseAnimating* CTerrorViewModel::FindFollowedEntity()
{
    if ( ViewModelIndex() == VMINDEX_HANDS )
    {
		C_TerrorPlayer* pPlayer = static_cast<C_TerrorPlayer*>( GetOwner() );

        if ( pPlayer )
        {
            C_BaseViewModel* vm = pPlayer->GetViewModel( VMINDEX_WEP );

            if ( vm )
            {
                return vm;
            }
        }
    }

    return C_BaseAnimating::FindFollowedEntity();
}

void C_TerrorViewModelAddon::SetViewModel(C_TerrorViewModel *vm)
{
	SetOwner(vm->GetOwner());
	m_hViewModel = vm;
}

void C_TerrorViewModelAddon::UpdatePoseParams()
{
	C_BasePlayer *pOwner = static_cast<C_BasePlayer *>(m_hViewModel->GetOwner());
	if(!pOwner->IsLocalPlayer())
		return;

	const Vector &velocity = pOwner->GetAbsVelocity();

	Vector forward;
	pOwner->GetVectors(&forward, nullptr, nullptr);

	SetPoseParameter(m_nMoveX, velocity.Dot(forward));
}

bool C_TerrorViewModelAddon::InitializeAsClientEntity(const char *pszModelName, bool bRenderWithViewModels)
{
	if(BaseClass::InitializeAsClientEntity(pszModelName, bRenderWithViewModels)) {
		AddEffects(EF_BONEMERGE|EF_BONEMERGE_FASTCULL|EF_PARENT_ANIMATES);
		return true;
	}

	return false;
}

int C_TerrorViewModelAddon::InternalDrawModel(int flags, const RenderableInstance_t &instance)
{
	CMatRenderContextPtr pRenderContext(materials);

	if(m_hViewModel->ShouldFlipViewModel())
		pRenderContext->CullMode(MATERIAL_CULLMODE_CW);

	int ret = BaseClass::InternalDrawModel(flags, instance);

	pRenderContext->CullMode(MATERIAL_CULLMODE_CCW);

	return ret;
}

CStudioHdr *C_TerrorViewModelAddon::OnNewModel()
{
	CStudioHdr *hdr = BaseClass::OnNewModel();

	m_nMoveX = LookupPoseParameter(hdr, "move_x");
	m_nVertAims = LookupPoseParameter(hdr, "ver_aims");

	return hdr;
}

int C_TerrorViewModelAddon::DrawModel(int flags, const RenderableInstance_t &instance)
{
	if(!IsVisible())
		return 0;

	C_BasePlayer *pLocalPlayer = C_BasePlayer::GetLocalPlayer();
	C_BasePlayer *pPlayer = static_cast<C_BasePlayer *>(m_hViewModel->GetOwner());

	bool observer = pLocalPlayer->IsObserver();

	if(observer && (pLocalPlayer->GetObserverTarget() != pPlayer))
		return false;

	if(!observer && (pLocalPlayer != pPlayer))
		return false;

	return BaseClass::DrawModel(flags, instance);
}
#endif