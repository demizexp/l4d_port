#ifndef _L4DCE_GAMERULES_H_
#define _L4DCE_GAMERULES_H_

#pragma once

#include "gamerules.h"
#include "multiplay_gamerules.h"

#ifdef CLIENT_DLL
#define CTerrorGameRulesProxy C_TerrorGameRulesProxy
#define CTerrorGameRules C_TerrorGameRules
#endif

class CTerrorGameRulesProxy : public CGameRulesProxy
{
	public:
		DECLARE_CLASS(CTerrorGameRulesProxy, CGameRulesProxy);
		DECLARE_NETWORKCLASS();
		DECLARE_DATADESC();
};

enum TerrorGameState
{
	STATE_INVALID,

	STATE_LOBBY,
	STATE_INGAME,
};

class CTerrorGameRules : public CMultiplayRules
{
	public:
		DECLARE_CLASS(CTerrorGameRules, CMultiplayRules);
		DECLARE_NETWORKCLASS_NOBASE();

		CTerrorGameRules();
		virtual ~CTerrorGameRules();

		virtual const unsigned char *GetEncryptionKey();
		#ifdef GAME_DLL
		virtual const char *GetGameDescription();
		#endif

		TerrorGameState GetGameState() const;
		void SetGameState(TerrorGameState state);

		#ifdef GAME_DLL
		virtual void InitDefaultAIRelationships();
		virtual const char *AIClassText(int classType);
		virtual bool IsDeathmatch() { return false; }
		virtual bool IsTeamplay() { return false;  }
		virtual bool IsCoOp() { return true; }
		virtual void PlayerSpawn( CBasePlayer *pPlayer );
		#endif

		virtual void LevelInitPostEntity();

		virtual bool FAllowFlashlight(void);

	protected:
		TerrorGameState m_nGameState = STATE_INVALID;
};

CTerrorGameRules *TerrorGameRules();

#endif