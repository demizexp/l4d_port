#include "cbase.h"
#include "weapon_terror_base.h"
#include "tier0/memdbgon.h"

#ifdef CLIENT_DLL
#define CWeaponSMGSilenced C_WeaponSMGSilenced
#endif

class CWeaponSMGSilenced : public CTerrorWeaponBase
{
	private:
		DECLARE_CLASS(CWeaponSMGSilenced, CTerrorWeaponBase);
		DECLARE_NETWORKCLASS();
		DECLARE_PREDICTABLE();
		DECLARE_DATADESC();

	public:
		
};

LINK_ENTITY_TO_CLASS(weapon_smg_silenced, CWeaponSMGSilenced);
PRECACHE_WEAPON_REGISTER(weapon_smg_silenced);

BEGIN_NETWORK_TABLE(CWeaponSMGSilenced, DT_WeaponSMGSilenced)
END_NETWORK_TABLE();

IMPLEMENT_NETWORKCLASS_ALIASED(WeaponSMGSilenced, DT_WeaponSMGSilenced);

BEGIN_PREDICTION_DATA(CWeaponSMGSilenced)
END_PREDICTION_DATA();

BEGIN_DATADESC(CWeaponSMGSilenced)
END_DATADESC();
