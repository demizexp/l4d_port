#include "cbase.h"
#include "weapon_terror_melee_base.h"
#include "tier0/memdbgon.h"

#ifdef CLIENT_DLL
#define CWeaponClaw C_WeaponClaw
#endif

class CWeaponClaw : public CTerrorWeaponMeleeBase
{
private:
	DECLARE_CLASS(CWeaponClaw, CTerrorWeaponMeleeBase);
	DECLARE_NETWORKCLASS();
	DECLARE_PREDICTABLE();
	DECLARE_DATADESC();

public:
	virtual char const	*GetPrintName(void) const;
	virtual const char	*GetViewModel(int iViewModel) const;
	virtual void Precache();
	float		GetRange( void );
	float		GetFireRate( void );
	float		GetDamageForActivity( Activity hitActivity );
};

BEGIN_NETWORK_TABLE(CWeaponClaw, DT_WeaponClaw)
END_NETWORK_TABLE();

LINK_ENTITY_TO_CLASS(weapon_claw, CWeaponClaw);
PRECACHE_WEAPON_REGISTER(weapon_claw);

IMPLEMENT_NETWORKCLASS_ALIASED(WeaponClaw, DT_WeaponClaw);

BEGIN_PREDICTION_DATA(CWeaponClaw)
END_PREDICTION_DATA();

BEGIN_DATADESC(CWeaponClaw)
END_DATADESC();


const char *CWeaponClaw::GetPrintName(void) const
{
	CTerrorPlayer *pPlayer = GetPlayerOwner();
	if (pPlayer && pPlayer->GetTeamNumber() == TEAM_INFECTED)
		return "#Weapon_Claw";
	else
		return "#Weapon_Punch";
}

float CWeaponClaw::GetDamageForActivity(Activity hitActivity)
{
	CTerrorPlayer *pPlayer = GetPlayerOwner();
	if (pPlayer && pPlayer->GetTeamNumber() == TEAM_INFECTED)
		return 50.0f; //todo: override this on a per-type basis when shit is implemented
	else
		return 10.0f;
}

float CWeaponClaw::GetRange(void)
{
	CTerrorPlayer *pPlayer = GetPlayerOwner();
	if (pPlayer && pPlayer->GetTeamNumber() == TEAM_INFECTED)
		return 60.0f; //todo: override this on a per-type basis when shit is implemented
	else
		return 60.0f;
}

float CWeaponClaw::GetFireRate(void)
{
	CTerrorPlayer *pPlayer = GetPlayerOwner();
	if (pPlayer && pPlayer->GetTeamNumber() == TEAM_INFECTED)
		return 0.8f; //todo: override this on a per-type basis when shit is implemented
	else
		return 0.8f;
}

void CWeaponClaw::Precache()
{
	//A lot to precache!
	PrecacheModel("models/weapons/claws/v_claw_survivor.mdl");

	PrecacheModel("models/weapons/claws/v_claw_common.mdl");
	PrecacheModel("models/weapons/claws/v_claw_common_female.mdl");
	PrecacheModel("models/weapons/claws/v_claw_common_l4d1.mdl");

	PrecacheModel("models/weapons/claws/v_claw_boomer.mdl");
	PrecacheModel("models/weapons/claws/v_claw_boomer_female.mdl");
	PrecacheModel("models/weapons/claws/v_claw_boomer_l4d1.mdl");

	PrecacheModel("models/weapons/claws/v_claw_jockey.mdl");
	PrecacheModel("models/weapons/claws/v_claw_jockey_female.mdl");
	//PrecacheModel("models/weapons/claws/v_claw_jockey_l4d1.mdl");

	PrecacheModel("models/weapons/claws/v_claw_smoker.mdl");
	PrecacheModel("models/weapons/claws/v_claw_smoker_female.mdl");
	PrecacheModel("models/weapons/claws/v_claw_smoker_l4d1.mdl");

	PrecacheModel("models/weapons/claws/v_claw_hunter.mdl");
	PrecacheModel("models/weapons/claws/v_claw_hunter_female.mdl");
	PrecacheModel("models/weapons/claws/v_claw_hunter_l4d1.mdl");

	PrecacheModel("models/weapons/claws/v_claw_spitter.mdl");
	PrecacheModel("models/weapons/claws/v_claw_spitter_female.mdl");
	//PrecacheModel("models/weapons/claws/v_claw_spitter_l4d1.mdl");

	PrecacheModel("models/weapons/claws/v_claw_tank.mdl");
	PrecacheModel("models/weapons/claws/v_claw_tank_female.mdl");
	PrecacheModel("models/weapons/claws/v_claw_tank_l4d1.mdl");

	PrecacheModel("models/weapons/claws/v_claw_witch.mdl");
	PrecacheModel("models/weapons/claws/v_claw_witch_female.mdl");
	PrecacheModel("models/weapons/claws/v_claw_witch_l4d1.mdl");

	PrecacheModel("models/weapons/claws/v_claw_charger.mdl");
	PrecacheModel("models/weapons/claws/v_claw_charger_female.mdl");
	//PrecacheModel("models/weapons/claws/v_claw_charger_l4d1.mdl");

	PrecacheModel("models/weapons/claws/v_claw_screamer.mdl");
	PrecacheModel("models/weapons/claws/v_claw_screamer_female.mdl");
	//PrecacheModel("models/weapons/claws/v_claw_screamer_l4d1.mdl");

	PrecacheModel("models/weapons/claws/v_claw_posioner.mdl");
	PrecacheModel("models/weapons/claws/v_claw_posioner_female.mdl");
	//PrecacheModel("models/weapons/claws/v_claw_posioner_l4d1.mdl");

	BaseClass::Precache();
}


//This is going to be a messy function... BLEH. Most of it isn't even ready!
const char *CWeaponClaw::GetViewModel(int iViewModel) const
{
	//First, lets get us a pointer to the player class.
	CTerrorPlayer *pPlayer = GetPlayerOwner();

	//No player? How'd we get here?
	if (!pPlayer)
		return "models/weapons/claws/v_claw_common.mdl";

	//Lets rule out survivors. If they somehow got the claws as one, let them punch shit.
	if (pPlayer->GetTeamNumber() == TEAM_SURVIVOR)
		return "models/weapons/claws/v_claw_survivor.mdl";

	//Then, lets check for each special infected.
	char* clawmodel = nullptr;
	int type = 0; //pPlayer->GetInfectedType()

	switch (type)
	{
		default: clawmodel = "models/weapons/claws/v_claw_common"; break;
		case INFECTED_BOOMER: clawmodel = "models/weapons/claws/v_claw_boomer"; break;
		case INFECTED_JOCKEY: clawmodel = "models/weapons/claws/v_claw_jockey"; break;
		case INFECTED_SMOKER: clawmodel = "models/weapons/claws/v_claw_smoker"; break;
		case INFECTED_HUNTER: clawmodel = "models/weapons/claws/v_claw_hunter"; break;
		case INFECTED_SPITTER: clawmodel = "models/weapons/claws/v_claw_spitter"; break;
		case INFECTED_TANK: clawmodel = "models/weapons/claws/v_claw_tank"; break;
		case INFECTED_WITCH: clawmodel = "models/weapons/claws/v_claw_witch"; break;
		case INFECTED_CHARGER: clawmodel = "models/weapons/claws/v_claw_charger"; break;
		case INFECTED_SCREAMER: clawmodel = "models/weapons/claws/v_claw_screamer"; break;
		case INFECTED_POSIONER: clawmodel = "models/weapons/claws/v_claw_posioner"; break;
	}

	//If we somehow got here past that switch, just use the common arms.
	if (clawmodel == nullptr)
		clawmodel = "models/weapons/claws/v_claw_common";

	int style = 0; // pPlayer->GetInfectedStyle();
	char* returnclaw = nullptr;

	//Get the style of viewmodel arms we should use (l4d1, l4d2-male or l4d2-female)
	if (style == INFECTEDSTYLE_L4D1)
		V_snprintf(returnclaw, sizeof(returnclaw), clawmodel, "_l4d1.mdl");
	else if (style == INFECTEDSTYLE_FEMALE)
		V_snprintf(returnclaw, sizeof(returnclaw), clawmodel, "_female.mdl");
	else
		V_snprintf(returnclaw, sizeof(returnclaw), clawmodel, ".mdl");

	//Finally, return the exact claw model you should use.
	return returnclaw;
}
