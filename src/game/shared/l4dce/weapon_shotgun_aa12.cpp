#include "cbase.h"
#include "weapon_terror_shotgun_base.h"
#include "tier0/memdbgon.h"

#ifdef CLIENT_DLL
#define CWeaponShotgunAA12 C_WeaponShotgunAA12
#endif

class CWeaponShotgunAA12 : public CTerrorWeaponShotgunBase
{
	private:
		DECLARE_CLASS(CWeaponShotgunAA12, CTerrorWeaponShotgunBase);
		DECLARE_NETWORKCLASS();
		DECLARE_PREDICTABLE();
		DECLARE_DATADESC();
		
	public:
		CWeaponShotgunAA12();
};

CWeaponShotgunAA12::CWeaponShotgunAA12()
{
	m_bShotgunPumpAction = false;
	m_bShotgunAutomatic = true;
}

BEGIN_NETWORK_TABLE(CWeaponShotgunAA12, DT_WeaponShotgunAA12)
END_NETWORK_TABLE();

IMPLEMENT_NETWORKCLASS_ALIASED(WeaponShotgunAA12, DT_WeaponShotgunAA12);

BEGIN_PREDICTION_DATA(CWeaponShotgunAA12)
END_PREDICTION_DATA();

LINK_ENTITY_TO_CLASS(weapon_shotgun_aa12, CWeaponShotgunAA12);
PRECACHE_WEAPON_REGISTER(weapon_shotgun_aa12);

BEGIN_DATADESC(CWeaponShotgunAA12)
END_DATADESC();
