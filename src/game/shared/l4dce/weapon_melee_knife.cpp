#include "cbase.h"
#include "weapon_terror_melee_base.h"
#include "tier0/memdbgon.h"

#ifdef CLIENT_DLL
#define CWeaponMeleeKnife C_WeaponMeleeKnife
#endif

class CWeaponMeleeKnife : public CTerrorWeaponMeleeBase
{
private:
	DECLARE_CLASS(CWeaponMeleeKnife, CTerrorWeaponMeleeBase);
	DECLARE_NETWORKCLASS();
	DECLARE_PREDICTABLE();
	DECLARE_DATADESC();

public:

};

BEGIN_NETWORK_TABLE(CWeaponMeleeKnife, DT_WeaponMeleeKnife)
END_NETWORK_TABLE();

LINK_ENTITY_TO_CLASS(weapon_melee_knife, CWeaponMeleeKnife);
PRECACHE_WEAPON_REGISTER(weapon_melee_knife);

IMPLEMENT_NETWORKCLASS_ALIASED(WeaponMeleeKnife, DT_WeaponMeleeKnife);

BEGIN_PREDICTION_DATA(CWeaponMeleeKnife)
END_PREDICTION_DATA();

BEGIN_DATADESC(CWeaponMeleeKnife)
END_DATADESC();

