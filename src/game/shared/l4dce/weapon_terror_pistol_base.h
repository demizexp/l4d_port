#ifndef _TERROR_WEAPON_PISTOL_BASE_H_
#define _TERROR_WEAPON_PISTOL_BASE_H_

#pragma once

#include "weapon_terror_base.h"

#ifdef CLIENT_DLL
#define CTerrorWeaponPistolBase C_TerrorWeaponPistolBase
#endif

class CTerrorWeaponPistolBase : public CTerrorWeaponBase
{
private:
	DECLARE_CLASS(CTerrorWeaponPistolBase, CTerrorWeaponBase);
	DECLARE_NETWORKCLASS();
	DECLARE_PREDICTABLE();
	DECLARE_ACTTABLE();
	DECLARE_DATADESC();

public:
	void PrimaryAttack();
	bool Reload();
};

#endif