#include "cbase.h"
#include "weapon_terror_tactscope_base.h"
#include "tier0/memdbgon.h"

#ifdef CLIENT_DLL
#define CWeaponRifleSG552 C_WeaponRifleSG552
#endif

class CWeaponRifleSG552 : public CTerrorWeaponTactScopeBase
{
	private:
		DECLARE_CLASS(CWeaponRifleSG552, CTerrorWeaponTactScopeBase);
		DECLARE_NETWORKCLASS();
		DECLARE_PREDICTABLE();
		DECLARE_ACTTABLE();
		DECLARE_DATADESC();

public:
		
};

BEGIN_NETWORK_TABLE(CWeaponRifleSG552, DT_WeaponRifleSG552)
END_NETWORK_TABLE();

LINK_ENTITY_TO_CLASS(weapon_rifle_sg552, CWeaponRifleSG552);
PRECACHE_WEAPON_REGISTER(weapon_rifle_sg552);

IMPLEMENT_NETWORKCLASS_ALIASED(WeaponRifleSG552, DT_WeaponRifleSG552);

BEGIN_PREDICTION_DATA(CWeaponRifleSG552)
END_PREDICTION_DATA();

BEGIN_DATADESC(CWeaponRifleSG552)
END_DATADESC();

acttable_t CWeaponRifleSG552::m_acttable[] =
{
	{ ACT_MP_STAND_IDLE,	ACT_IDLE_RIFLE, false },
	{ ACT_MP_CROUCH_IDLE,	ACT_CROUCHIDLE_RIFLE, false },
	{ ACT_MP_RUN,			ACT_RUN_RIFLE, false },
	{ ACT_MP_CROUCHWALK,	ACT_RUN_CROUCH_RIFLE, false },
	{ ACT_MP_JUMP,			ACT_JUMP_RIFLE, false },
	{ ACT_MP_RELOAD_STAND,	ACT_RELOAD_RIFLE, false },
	{ ACT_MP_RELOAD_CROUCH, ACT_RELOAD_RIFLE, false },
	{ ACT_RANGE_ATTACK1,	ACT_PRIMARYATTACK_RIFLE, false },
};
IMPLEMENT_ACTTABLE(CWeaponRifleSG552);

