#include "cbase.h"
#include "weapon_terror_shotgun_base.h"
#include "tier0/memdbgon.h"

#ifdef CLIENT_DLL
#define CWeaponShotgunSpas C_WeaponShotgunSpas
#endif

class CWeaponShotgunSpas : public CTerrorWeaponShotgunBase
{
private:
	DECLARE_CLASS(CWeaponShotgunSpas, CTerrorWeaponShotgunBase);
	DECLARE_NETWORKCLASS();
	DECLARE_PREDICTABLE();
	DECLARE_DATADESC();
	
	public:
		CWeaponShotgunSpas();
};

CWeaponShotgunSpas::CWeaponShotgunSpas()
{
	m_bShotgunPumpAction = false;
	m_bShotgunAutomatic = false;
}

BEGIN_NETWORK_TABLE(CWeaponShotgunSpas, DT_WeaponShotgunSpas)
END_NETWORK_TABLE();

IMPLEMENT_NETWORKCLASS_ALIASED(WeaponShotgunSpas, DT_WeaponShotgunSpas);

BEGIN_PREDICTION_DATA(CWeaponShotgunSpas)
END_PREDICTION_DATA();

LINK_ENTITY_TO_CLASS(weapon_shotgun_spas, CWeaponShotgunSpas);
PRECACHE_WEAPON_REGISTER(weapon_shotgun_spas);

BEGIN_DATADESC(CWeaponShotgunSpas)
END_DATADESC();
