#include "cbase.h"
#include "ammodef.h"
#include "l4dce_gamerules.h"
#include "l4dce_shareddefs.h"
#include "game_timescale_shared.h"
#ifdef CLIENT_DLL
#include "voice_status.h"
#else
#include "shareddefs.h"
#include "basecombatcharacter.h"
#include "team.h"
#include "l4dce_info_director.h"
#endif
#include "voice_gamemgr.h"
#include "l4dce_player_shared.h"
#include "tier0/memdbgon.h"

ConVar mp_gamemode("mp_gamemode", "campaign", FCVAR_REPLICATED|FCVAR_DEVELOPMENTONLY, "Current game mode, acceptable values are campaign and single_mission.", false, 0.0f, false, 0.0f);

REGISTER_GAMERULES_CLASS(CTerrorGameRules);

BEGIN_NETWORK_TABLE_NOBASE(CTerrorGameRules, DT_TerrorGameRules)
END_NETWORK_TABLE();

BEGIN_DATADESC(CTerrorGameRulesProxy)
END_DATADESC();

LINK_ENTITY_TO_CLASS(terror_gamerules, CTerrorGameRulesProxy);
IMPLEMENT_NETWORKCLASS_ALIASED(TerrorGameRulesProxy, DT_TerrorGameRulesProxy)

#ifdef CLIENT_DLL
void RecvProxy_TerrorGameRules(const RecvProp *pProp, void **pOut, void *pData, int objectID)
{
	CTerrorGameRules *pRules = TerrorGameRules();
	Assert(pRules);
	*pOut = pRules;
}

BEGIN_RECV_TABLE(CTerrorGameRulesProxy, DT_TerrorGameRulesProxy)
	RecvPropDataTable("terror_gamerules_data", 0, 0, &REFERENCE_RECV_TABLE(DT_TerrorGameRules), RecvProxy_TerrorGameRules),
END_RECV_TABLE()
#else
void *SendProxy_TerrorGameRules(const SendProp *pProp, const void *pStructBase, const void *pData, CSendProxyRecipients *pRecipients, int objectID)
{
	CTerrorGameRules *pRules = TerrorGameRules();
	Assert(pRules);
	pRecipients->SetAllRecipients();
	return pRules;
}

BEGIN_SEND_TABLE(CTerrorGameRulesProxy, DT_TerrorGameRulesProxy)
	SendPropDataTable("terror_gamerules_data", 0, &REFERENCE_SEND_TABLE(DT_TerrorGameRules), SendProxy_TerrorGameRules)
END_SEND_TABLE()
#endif

#define BULLET_MASS_GRAINS_TO_LB(grains)	(0.002285*(grains)/16.0f)
#define BULLET_MASS_GRAINS_TO_KG(grains)	lbs2kg(BULLET_MASS_GRAINS_TO_LB(grains))

#define BULLET_IMPULSE_EXAGGERATION			3.5
#define BULLET_IMPULSE(grains, ftpersec)	((ftpersec)*12*BULLET_MASS_GRAINS_TO_KG(grains)*BULLET_IMPULSE_EXAGGERATION)

static ConVar ammo_pistol_max("ammo_pistol_max", "-2", FCVAR_REPLICATED);
static ConVar ammo_assaultrifle_max("ammo_assaultrifle_max", "360", FCVAR_REPLICATED);
static ConVar ammo_minigun_max("ammo_minigun_max", "800", FCVAR_REPLICATED);
static ConVar ammo_smg_max("ammo_smg_max", "650", FCVAR_REPLICATED);
static ConVar ammo_m60_max("ammo_m60_max", "0", FCVAR_REPLICATED);
static ConVar ammo_shotgun_max("ammo_shotgun_max", "56", FCVAR_REPLICATED);
static ConVar ammo_autoshotgun_max("ammo_autoshotgun_max", "90", FCVAR_REPLICATED);
static ConVar ammo_huntingrifle_max("ammo_huntingrifle_max", "150", FCVAR_REPLICATED);
static ConVar ammo_sniperrifle_max("ammo_sniperrifle_max", "180", FCVAR_REPLICATED);
static ConVar ammo_turret_max("ammo_turret_max", "300", FCVAR_REPLICATED);
static ConVar ammo_pipebomb_max("ammo_pipebomb_max", "1", FCVAR_REPLICATED);
static ConVar ammo_molotov_max("ammo_molotov_max", "1", FCVAR_REPLICATED);
static ConVar ammo_vomitjar_max("ammo_vomitjar_max", "1", FCVAR_REPLICATED);
static ConVar ammo_painpills_max("ammo_painpills_max", "1", FCVAR_REPLICATED);
static ConVar ammo_firstaid_max("ammo_firstaid_max", "1", FCVAR_REPLICATED);
static ConVar ammo_grenadelauncher_max("ammo_grenadelauncher_max", "30", FCVAR_REPLICATED);
static ConVar ammo_adrenaline_max("ammo_adrenaline_max", "1", FCVAR_REPLICATED);
static ConVar ammo_chainsaw_max("ammo_chainsaw_max", "20", FCVAR_REPLICATED);
//static ConVar ammo_ammo_pack_max("ammo_ammo_pack_max", "1", FCVAR_REPLICATED);

CAmmoDef *GetAmmoDef()
{
	static CAmmoDef def;
	static bool bInitted = false;
	if(!bInitted)
	{
		bInitted = true;
		def.AddAmmoType("AMMO_TYPE_PISTOL", DMG_BULLET, TRACER_LINE, nullptr, nullptr, "ammo_pistol_max", BULLET_IMPULSE(200, 1225), 0, 10, 14);
		def.AddAmmoType("AMMO_TYPE_PISTOL_MAGNUM", DMG_BULLET, TRACER_LINE, nullptr, nullptr, "ammo_pistol_max", BULLET_IMPULSE(800, 5000), 0, 10, 14);
		def.AddAmmoType("AMMO_TYPE_ASSAULTRIFLE", DMG_BULLET, TRACER_LINE, nullptr, nullptr, "ammo_assaultrifle_max", BULLET_IMPULSE(200, 1225), 0, 10, 14);
		def.AddAmmoType("AMMO_TYPE_MINIGUN", DMG_BULLET, TRACER_LINE, nullptr, nullptr, "ammo_minigun_max", BULLET_IMPULSE(200, 1225), 0, 10, 14);
		def.AddAmmoType("AMMO_TYPE_SMG", DMG_BULLET, TRACER_LINE, nullptr, nullptr, "ammo_smg_max", BULLET_IMPULSE(200, 1225), 0, 5, 10);
		def.AddAmmoType("AMMO_TYPE_M60", DMG_BULLET, TRACER_LINE, nullptr, nullptr, "ammo_m60_max", BULLET_IMPULSE(200, 1225), 0, 10, 14);
		def.AddAmmoType("AMMO_TYPE_SHOTGUN", DMG_BULLET|DMG_BUCKSHOT, TRACER_LINE, nullptr, nullptr, "ammo_shotgun_max", BULLET_IMPULSE(400, 1200), 0, 3, 6);
		def.AddAmmoType("AMMO_TYPE_AUTOSHOTGUN", DMG_BULLET|DMG_BUCKSHOT, TRACER_LINE, nullptr, nullptr, "ammo_autoshotgun_max", BULLET_IMPULSE(400, 1200), 0, 3, 6);
		def.AddAmmoType("AMMO_TYPE_HUNTINGRIFLE", DMG_BULLET, TRACER_LINE, nullptr, nullptr, "ammo_huntingrifle_max", BULLET_IMPULSE(150, 6000), 0, 10, 14);
		def.AddAmmoType("AMMO_TYPE_SNIPERRIFLE", DMG_BULLET, TRACER_LINE, nullptr, nullptr, "ammo_sniperrifle_max", BULLET_IMPULSE(150, 6000), 0, 10, 14);
		def.AddAmmoType("AMMO_TYPE_TURRET", DMG_BULLET, TRACER_LINE, nullptr, nullptr, "ammo_turret_max", BULLET_IMPULSE(200, 1225), 0, 10, 14);
		def.AddAmmoType("AMMO_TYPE_PIPEBOMB", DMG_BLAST|DMG_ALWAYSGIB, TRACER_NONE, nullptr, nullptr, "ammo_pipebomb_max", 0.0f, 0, 0, 0);
		def.AddAmmoType("AMMO_TYPE_MOLOTOV", DMG_BLAST|DMG_SLOWBURN|DMG_BURN, TRACER_NONE, nullptr, nullptr, "ammo_molotov_max", 0.0f, 0, 0, 0);
		def.AddAmmoType("AMMO_TYPE_VOMITJAR", DMG_BLAST|DMG_POISON|DMG_NERVEGAS|DMG_ACID|DMG_PARALYZE, TRACER_NONE, nullptr, nullptr, "ammo_vomitjar_max", 0.0f, 0, 0, 0);
		def.AddAmmoType("AMMO_TYPE_PAINPILLS", DMG_GENERIC, TRACER_NONE, nullptr, nullptr, "ammo_painpills_max", 0.0f, 0, 0, 0);
		def.AddAmmoType("AMMO_TYPE_FIRSTAID", DMG_GENERIC, TRACER_NONE, nullptr, nullptr, "ammo_firstaid_max", 0.0f, 0, 0, 0);
		def.AddAmmoType("AMMO_TYPE_GRENADELAUNCHER", DMG_BLAST|DMG_ALWAYSGIB, TRACER_NONE, nullptr, nullptr, "ammo_grenadelauncher_max", 0.0f, 0, 0, 0);
		def.AddAmmoType("AMMO_TYPE_ADRENALINE", DMG_GENERIC, TRACER_NONE, nullptr, nullptr, "ammo_adrenaline_max", 0.0f, 0, 0, 0);
		def.AddAmmoType("AMMO_TYPE_CHAINSAW", DMG_GENERIC, TRACER_NONE, nullptr, nullptr, "ammo_chainsaw_max", 0.0f, 0, 0, 0);
		def.AddAmmoType("AMMO_CARRIED_ITEM", DMG_GENERIC, TRACER_NONE, 0, 0, 1, 0.0f, 0, 0, 0);
	}

	return &def;
}

CTerrorGameRules::CTerrorGameRules()
{
	#ifdef GAME_DLL
	static const char *const teamnames[] = {
		"Unassigned",
		"Spectator",
		"Survivors",
		"Infected",
	};
	for(int i = 0; i < ARRAYSIZE(teamnames); i++) {
		CTeam *pTeam = static_cast<CTeam *>(CreateEntityByName("team_manager"));
		DispatchSpawn(pTeam);
		pTeam->Init(teamnames[i], i);
		g_Teams.AddToTail(pTeam);
	}
	#else
	engine->SetPitchScale(1.0f);

	CVoiceStatus *pVoiceMgr = GetClientVoiceMgr();
	if(pVoiceMgr)
		pVoiceMgr->SetHeadLabelsDisabled(true);
	#endif
}

CTerrorGameRules::~CTerrorGameRules()
{
	#ifdef GAME_DLL
	g_Teams.Purge();
	#endif

	GameTimescale()->SetDesiredTimescale(1.0f);

	#ifdef CLIENT_DLL
	engine->SetPitchScale(1.0f);
	#endif
}

const unsigned char *CTerrorGameRules::GetEncryptionKey()
{
	return (const unsigned char *)"d7NSuLq2";
}

#ifdef GAME_DLL
class CVoiceGameMgrHelper : public IVoiceGameMgrHelper
{
	public:
		virtual bool CanPlayerHearPlayer(CBasePlayer *pListener, CBasePlayer *pTalker, bool &bProximity);
};

bool CVoiceGameMgrHelper::CanPlayerHearPlayer(CBasePlayer *pListener, CBasePlayer *pTalker, bool &bProximity)
{
	return (pListener->GetTeamNumber() == pTalker->GetTeamNumber());
}

CVoiceGameMgrHelper g_VoiceGameMgrHelper;
IVoiceGameMgrHelper *g_pVoiceGameMgrHelper = &g_VoiceGameMgrHelper;

const char *CTerrorGameRules::GetGameDescription()
{
	return "Left 4 Dead: Community Edition";
}

void InitBodyQue()
{

}
#endif

TerrorGameState CTerrorGameRules::GetGameState() const
{
	return m_nGameState;
}

void CTerrorGameRules::SetGameState(TerrorGameState state)
{
	m_nGameState = state;
}

class CBaseCombatCharacter;

#ifdef GAME_DLL
void CTerrorGameRules::InitDefaultAIRelationships()
{
	CBaseCombatCharacter::AllocateDefaultRelationships();

	for(int i = 0; i < LAST_SHARED_ENTITY_CLASS; i++) {
		for(int j = 0; j < LAST_SHARED_ENTITY_CLASS; j++)
			CBaseCombatCharacter::SetDefaultRelationship((Class_T)i, (Class_T)j, D_NU, 0);
	}

	CBaseCombatCharacter::SetDefaultRelationship(CLASS_NONE, CLASS_NONE, D_NU, 0);
	CBaseCombatCharacter::SetDefaultRelationship(CLASS_NONE, CLASS_PLAYER, D_NU, 0);
	CBaseCombatCharacter::SetDefaultRelationship(CLASS_NONE, CLASS_ZOMBIE, D_NU, 0);

	CBaseCombatCharacter::SetDefaultRelationship(CLASS_PLAYER, CLASS_NONE, D_NU, 0);
	CBaseCombatCharacter::SetDefaultRelationship(CLASS_PLAYER, CLASS_PLAYER, D_LI, 0);
	CBaseCombatCharacter::SetDefaultRelationship(CLASS_PLAYER, CLASS_ZOMBIE, D_HT, 0);

	CBaseCombatCharacter::SetDefaultRelationship(CLASS_ZOMBIE, CLASS_NONE, D_NU, 0);
	CBaseCombatCharacter::SetDefaultRelationship(CLASS_ZOMBIE, CLASS_PLAYER, D_HT, 0);
	CBaseCombatCharacter::SetDefaultRelationship(CLASS_ZOMBIE, CLASS_ZOMBIE, D_LI, 0);
}

const char *CTerrorGameRules::AIClassText(int classType)
{
	switch(classType)
	{
		case CLASS_NONE: return "CLASS_NONE";
		case CLASS_PLAYER: return "CLASS_PLAYER";
		case CLASS_ZOMBIE: return "CLASS_ZOMBIE";
	}

	return nullptr;
}

void CTerrorGameRules::PlayerSpawn( CBasePlayer *pPlayer )
{
	BaseClass::PlayerSpawn( pPlayer );
	if ( GetGameState() == STATE_LOBBY )
	{
		if (CInfoDirector *pInfoDirector = (CInfoDirector *)gEntList.FindEntityByClassname(NULL, "info_director"))
			pInfoDirector->m_OnGamePlayStart.FireOutput(pInfoDirector, pInfoDirector);

		SetGameState(STATE_INGAME);
	}
}
#endif

void CTerrorGameRules::LevelInitPostEntity()
{
	BaseClass::LevelInitPostEntity();

	SetGameState(STATE_LOBBY);
}

CTerrorGameRules *TerrorGameRules()
{
	return static_cast<CTerrorGameRules *>(g_pGameRules);
}

bool CTerrorGameRules::FAllowFlashlight()
{
	for (int i = 1; i <= gpGlobals->maxClients; i++ )
	{
		CTerrorPlayer *pPlayer = (CTerrorPlayer*) UTIL_PlayerByIndex( i );
		if (pPlayer->GetTeamNumber() == TEAM_SURVIVOR)
			return true;
	}

	return false;
}
