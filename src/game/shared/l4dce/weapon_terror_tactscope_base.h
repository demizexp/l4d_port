#ifndef _TERROR_WEAPON_TACTSCOPE_BASE_H_
#define _TERROR_WEAPON_TACTSCOPE_BASE_H_

#pragma once

#include "weapon_terror_base.h"

#ifdef CLIENT_DLL
#define CTerrorWeaponTactScopeBase C_TerrorWeaponTactScopeBase
#endif

class CTerrorWeaponTactScopeBase : public CTerrorWeaponBase
{
private:
	DECLARE_CLASS(CTerrorWeaponTactScopeBase, CTerrorWeaponBase);
	DECLARE_NETWORKCLASS();
	DECLARE_PREDICTABLE();
	DECLARE_DATADESC();

public:
	CTerrorWeaponTactScopeBase();
	float GetFireRate();

	bool Holster(CBaseCombatWeapon *pSwitchingTo = NULL); 
	void ItemPostFrame();
	void ItemBusyFrame();
	bool Reload();
	void MeleeAttack();

protected:
	void ToggleZoom();
	void CheckZoomToggle();

	bool m_bInZoom;
};

#endif