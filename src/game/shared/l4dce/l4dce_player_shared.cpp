#include "cbase.h"
#include "l4dce_player_shared.h"
#include "tier0/memdbgon.h"

#ifdef CLIENT_DLL
#define CTerrorPlayer C_TerrorPlayer
#endif

ConVar debugarms("debug_arm_models", "0", FCVAR_CHEAT);

const char *CTerrorPlayer::GetHandModelName() const
{
	if (debugarms.GetBool())
		return "models/weapons/arms/v_arms_mechanic.mdl";

	return g_ppszArmModels[m_nSurvivor];
}


