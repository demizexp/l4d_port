#include "cbase.h"
#include "l4dce_player.h"
#include "l4dce_playeranimstate.h"
#include "datacache/imdlcache.h"
#include "tier0/memdbgon.h"

CTerrorPlayerAnimState *CreateTerrorPlayerAnimState(CTerrorPlayer *pPlayer)
{
	MDLCACHE_CRITICAL_SECTION();

	MultiPlayerMovementData_t movementData;
	movementData.m_flBodyYawRate = 720.0f;
	movementData.m_flRunSpeed = 150.0f;
	movementData.m_flWalkSpeed = 80.0f;
	movementData.m_flSprintSpeed = -1.0f;

	CTerrorPlayerAnimState *pRet = new CTerrorPlayerAnimState(pPlayer, movementData);
	pRet->InitTerror(pPlayer);
	
	return pRet;
}

CTerrorPlayerAnimState::CTerrorPlayerAnimState(CTerrorPlayer *pPlayer, MultiPlayerMovementData_t &data)
	: BaseClass(pPlayer, data), m_pPlayer(pPlayer)
{
	
}

void CTerrorPlayerAnimState::InitTerror(CTerrorPlayer *pPlayer)
{
	m_pPlayer = pPlayer;
}

void CTerrorPlayerAnimState::ComputePoseParam_AimPitch(CStudioHdr *pStudioHdr)
{
	m_pPlayer->SetPoseParameter(pStudioHdr, m_PoseParameterData.m_iAimPitch, m_flEyePitch);
	m_DebugAnimData.m_flAimPitch = m_flEyePitch;
}

void CTerrorPlayerAnimState::ComputePoseParam_AimYaw(CStudioHdr *pStudioHdr)
{
	Vector vecVelocity;
	GetOuterAbsVelocity(vecVelocity);

	bool bMoving = (vecVelocity.Length() > 1.0f) ? true : false;

	if(bMoving || m_bForceAimYaw)
		m_flGoalFeetYaw = m_flEyeYaw;
	else {
		if(m_PoseParameterData.m_flLastAimTurnTime <= 0.0f) {
			m_flGoalFeetYaw = m_flEyeYaw;
			m_flCurrentFeetYaw = m_flEyeYaw;
			m_PoseParameterData.m_flLastAimTurnTime = gpGlobals->curtime;
		} else {
			float flYawDelta = AngleNormalize(m_flGoalFeetYaw - m_flEyeYaw);
			if(fabs(flYawDelta) > 45.0f) {
				float flSide = (flYawDelta > 0.0f) ? -1.0f : 1.0f;
				m_flGoalFeetYaw += (45.0f * flSide);
			}
		}
	}

	m_flGoalFeetYaw = AngleNormalize(m_flGoalFeetYaw);
	if(m_flGoalFeetYaw != m_flCurrentFeetYaw) {
		if(m_bForceAimYaw)
			m_flCurrentFeetYaw = m_flGoalFeetYaw;
		else {
			ConvergeYawAngles(m_flGoalFeetYaw, 720.0f, gpGlobals->frametime, m_flCurrentFeetYaw);
			m_flLastAimTurnTime = gpGlobals->curtime;
		}
	}

	m_angRender[YAW] = m_flCurrentFeetYaw;

	float flAimYaw = m_flEyeYaw - m_flCurrentFeetYaw;
	flAimYaw = AngleNormalize(flAimYaw);

	m_pPlayer->SetPoseParameter(pStudioHdr, m_PoseParameterData.m_iAimYaw, flAimYaw);
	m_DebugAnimData.m_flAimYaw = flAimYaw;

	m_bForceAimYaw = false;

	#ifndef CLIENT_DLL
	QAngle angle = m_pPlayer->GetAbsAngles();
	angle[YAW] = m_flCurrentFeetYaw;
	m_pPlayer->SetAbsAngles(angle);
	#endif
}

Activity CTerrorPlayerAnimState::TranslateActivity(Activity actDesired)
{
	actDesired = BaseClass::TranslateActivity(actDesired);

	CBaseCombatWeapon *pWeapon = m_pPlayer->GetActiveWeapon();
	if(pWeapon)
		actDesired = pWeapon->ActivityOverride(actDesired, nullptr);

	return actDesired;
}

void CTerrorPlayerAnimState::Update(float eyeYaw, float eyePitch)
{
	BaseClass::Update(eyeYaw, eyePitch);

	float speed = GetCurrentMaxGroundSpeed();
	if(speed > 0.0f)
		m_pPlayer->SetMaxSpeed(speed);
}