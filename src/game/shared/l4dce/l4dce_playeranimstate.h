#ifndef _L4DCE_PLAYERANIMSTATE_H_
#define _L4DCE_PLAYERANIMSTATE_H_

#pragma once

#include "Multiplayer/multiplayer_animstate.h"

#ifdef CLIENT_DLL
#define CTerrorPlayerAnimState C_TerrorPlayerAnimState
#define CTerrorPlayer C_TerrorPlayer
#endif

class CTerrorPlayer;

class CTerrorPlayerAnimState : public CMultiPlayerAnimState
{
	private:
		DECLARE_CLASS(CTerrorPlayerAnimState, CMultiPlayerAnimState);

	public:
		CTerrorPlayerAnimState(CTerrorPlayer *pPlayer, MultiPlayerMovementData_t &data);

		virtual void ComputePoseParam_AimPitch(CStudioHdr *pStudioHdr);
		virtual void ComputePoseParam_AimYaw(CStudioHdr *pStudioHdr);
		virtual Activity TranslateActivity(Activity actDesired);
		virtual void Update(float eyeYaw, float eyePitch);

		void InitTerror(CTerrorPlayer *pPlayer);

	private:
		CTerrorPlayer *m_pPlayer = nullptr;
};

CTerrorPlayerAnimState *CreateTerrorPlayerAnimState(CTerrorPlayer *pPlayer);

#endif