#ifndef _L4DCE_PLAYER_H_
#define _L4DCE_PLAYER_H_

#include "c_baseplayer.h"
#include "l4dce_shareddefs.h"
#include "l4dce_gamerules.h"

class C_TerrorWeaponBase;

enum PlayerAnimEvent_t;
class C_TerrorPlayerAnimState;

class C_TerrorPlayer : public C_BasePlayer
{
	public:
		DECLARE_CLASS(C_TerrorPlayer, C_BasePlayer);
		DECLARE_CLIENTCLASS();
		DECLARE_PREDICTABLE();
		DECLARE_INTERPOLATION();

		C_TerrorPlayer();
		virtual ~C_TerrorPlayer();

		static C_TerrorPlayer *GetLocalPlayer(int nSlot = -1);

		void SetSurvivor(TerrorSurvivors survivor);
		TerrorSurvivors GetSurvivor() const;
		virtual void SetAnimation(PLAYER_ANIM playerAnim);
		virtual void DoAnimationEvent(PlayerAnimEvent_t event, int nData=0);
		virtual bool ShouldRegenerateOriginFromCellBits() const;
		virtual const QAngle &EyeAngles();
		virtual const QAngle &GetRenderAngles();
		virtual CStudioHdr *OnNewModel();
		virtual void UpdateClientSideAnimation();
		virtual void PostDataUpdate(DataUpdateType_t updateType);
		virtual void OnDataChanged(DataUpdateType_t updateType);
		bool IsWeaponLowered(void);
		C_TerrorWeaponBase *GetActiveTerrorWeapon(void) const;
		static C_TerrorPlayer* GetLocalTerrorPlayer();
		const char *GetHandModelName(void) const;

		/*
		virtual const char *GetFlashlightTextureName() const;
		virtual float GetFlashlightFOV() const;
		virtual float GetFlashlightFarZ() const;
		virtual float GetFlashlightLinearAtten() const;
		virtual bool CastsFlashlightShadows() const;
		*/

	public:
		C_TerrorPlayerAnimState *m_pAnimState = nullptr;
		QAngle m_angEyeAngles;
		CInterpolatedVar<QAngle> m_iv_angEyeAngles;
		TerrorSurvivors m_nSurvivor = SURVIVOR_INVALID;
		const char *m_szArmName;
};

C_TerrorPlayer *ToTerrorPlayer(C_BaseEntity *pEntity);

#endif