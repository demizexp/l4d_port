#ifndef _INCLUDED_C_ASW_FX_H
#define _INCLUDED_C_ASW_FX_H

#include "fx.h"

void FX_ASW_Potential_Burst_Pipe( const Vector &vecImpactPoint, const Vector &vecReflect, const Vector &vecShotBackward, const Vector &vecNormal );

#endif // _INCLUDED_C_ASW_FX_H