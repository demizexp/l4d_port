#ifndef _L4DCE_PLAYERSELECT_H_
#define _L4DCE_PLAYERSELECT_H_

#pragma once

#include "vgui_controls/EditablePanel.h"
#include "game_controls/basemodel_panel.h"
#include "game_controls/basemodelpanel.h"
#include "l4dce_shareddefs.h"

class CPlayerModelViewer : public CBaseModelPanel
{
	private:
		DECLARE_CLASS_SIMPLE(CPlayerModelViewer, CBaseModelPanel);

	public:
		CPlayerModelViewer(vgui::Panel *pParent, TerrorSurvivors survivor);
		virtual ~CPlayerModelViewer();

	private:
		virtual void PerformLayout();

		const Vector &GetCamPosOffset();
		const QAngle &GetCamAngOffset();

	private:
		TerrorSurvivors m_nSurvivor = SURVIVOR_INVALID;
};

class CPlayerSelect : public vgui::EditablePanel
{
	private:
		DECLARE_CLASS_SIMPLE(CPlayerSelect, vgui::EditablePanel);

	public:
		CPlayerSelect();
		virtual ~CPlayerSelect();

		virtual void InvalidateLayout(bool layoutNow = false, bool reloadScheme = false);

	private:
		virtual void PerformLayout();

	private:
		CUtlVector<CPlayerModelViewer *> m_ModelsPanelList;
		vgui::Label *m_pScreenText = nullptr;
		vgui::ImagePanel *m_pBackground = nullptr;
};

#endif