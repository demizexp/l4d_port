#ifndef _NEXT_BOT_H_
#define _NEXT_BOT_H_

#pragma once

#include "c_basecombatcharacter.h"

class C_NextBotCombatCharacter : public C_BaseCombatCharacter
{
	private:
		DECLARE_CLASS(C_NextBotCombatCharacter, C_BaseCombatCharacter);
		DECLARE_CLIENTCLASS();

	public:
		C_NextBotCombatCharacter();
		virtual ~C_NextBotCombatCharacter();

	private:
		
};

#endif